# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170505022141) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "appy_activities", force: true do |t|
    t.integer  "person_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.integer  "device_id"
    t.string   "activity_name"
    t.integer  "activity_id"
    t.string   "activity_type"
    t.text     "additional_info"
    t.datetime "created_at"
    t.string   "description"
    t.integer  "earned_points"
    t.integer  "spent_points"
    t.boolean  "active"
  end

  add_index "appy_activities", ["activity_id", "activity_type"], name: "index_appy_activities_on_activity_id_and_activity_type", using: :btree
  add_index "appy_activities", ["activity_name"], name: "index_appy_activities_on_activity_name", using: :btree
  add_index "appy_activities", ["appable_id", "appable_type"], name: "index_appy_activities_on_appable_id_and_appable_type", using: :btree
  add_index "appy_activities", ["device_id"], name: "index_appy_activities_on_device_id", using: :btree
  add_index "appy_activities", ["person_id"], name: "index_appy_activities_on_person_id", using: :btree

  create_table "appy_agreements", force: true do |t|
    t.integer  "level"
    t.integer  "amount"
    t.string   "currency"
    t.string   "payment_id"
    t.string   "future_pay_type"
    t.string   "future_pay_id"
    t.integer  "option"
    t.datetime "start_date"
    t.integer  "start_delay_unit"
    t.integer  "start_delay_mult"
    t.integer  "interval_unit"
    t.integer  "interval_mult"
    t.integer  "normal_amount"
    t.integer  "initial_amount"
    t.boolean  "cancelled",        default: false
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "plan_ref"
    t.string   "status",           default: "pending"
    t.datetime "expiration_date"
    t.string   "next_plan_ref"
    t.integer  "overload_price"
  end

  create_table "appy_api_keys", force: true do |t|
    t.string  "label",                                  null: false
    t.string  "api_key",                                null: false
    t.string  "platform",                               null: false
    t.string  "version",                                null: false
    t.string  "target_apps",            default: [],                 array: true
    t.string  "target_groups",          default: [],                 array: true
    t.integer "download_app_count",     default: 0
    t.integer "refresh_app_count",      default: 0
    t.integer "auto_refresh_app_count", default: 0
    t.integer "feedback_count",         default: 0
    t.integer "survey_count",           default: 0
    t.integer "booking_count",          default: 0
    t.boolean "master",                 default: false
    t.boolean "active",                 default: true
    t.date    "expiry_date"
    t.integer "appable_id"
    t.string  "appable_type"
  end

  add_index "appy_api_keys", ["api_key"], name: "index_appy_api_keys_on_api_key", using: :btree
  add_index "appy_api_keys", ["appable_id", "appable_type"], name: "index_appy_api_keys_on_appable_id_and_appable_type", using: :btree
  add_index "appy_api_keys", ["master"], name: "index_appy_api_keys_on_master", using: :btree
  add_index "appy_api_keys", ["target_apps"], name: "index_appy_api_keys_on_target_apps", using: :btree
  add_index "appy_api_keys", ["target_groups"], name: "index_appy_api_keys_on_target_groups", using: :btree

  create_table "appy_api_statistics", force: true do |t|
    t.integer "device_id",   null: false
    t.string  "api_key",     null: false
    t.string  "target_type"
    t.integer "target_id"
    t.string  "version",     null: false
    t.string  "url",         null: false
    t.string  "method",      null: false
  end

  create_table "appy_app_translations", force: true do |t|
    t.string   "website"
    t.integer  "app_id"
    t.string   "phone"
    t.string   "fax"
    t.string   "calling_code"
    t.string   "facebook_page_url"
    t.string   "twitter_handle"
    t.string   "closest_large_city"
    t.string   "address_line_one"
    t.string   "welcome_line_one"
    t.string   "address_line_two"
    t.string   "welcome_line_two"
    t.string   "address_line_three"
    t.string   "welcome_line_three"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "language_id"
    t.text     "booking_url"
    t.string   "booking_url_protocol"
    t.string   "website_protocol"
    t.string   "facebook_page_id"
    t.string   "description",          limit: 1000
    t.string   "weibo",                limit: 500
    t.string   "youtube",              limit: 500
    t.string   "youku",                limit: 500
  end

  add_index "appy_app_translations", ["language_id"], name: "index_appy_app_translations_on_language_id", using: :btree

  create_table "appy_apps", force: true do |t|
    t.integer  "appy_id"
    t.integer  "enabled_design_id"
    t.integer  "user_id"
    t.string   "name"
    t.string   "city"
    t.string   "country"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.boolean  "active",                                                default: true
    t.text     "note"
    t.boolean  "allow_search",                                          default: true
    t.boolean  "display_logo_badge",                                    default: true
    t.string   "subcontinent"
    t.boolean  "visible_to_device",                                     default: false
    t.datetime "sent_no_change_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency",                                              default: "USD"
    t.string   "plan_ref"
    t.string   "orders_cc"
    t.string   "bookings_cc"
    t.string   "timezone_id"
    t.boolean  "deleted",                                               default: false
    t.boolean  "on_search",                                             default: false
    t.boolean  "allow_room_control",                                    default: false
    t.string   "siteminder_id"
    t.string   "mobile_domain_name"
    t.decimal  "lat",                         precision: 18, scale: 15
    t.decimal  "lng",                         precision: 18, scale: 15
    t.boolean  "automation_mode",                                       default: false
    t.text     "email_subscribe"
    t.integer  "geo_continent_id"
    t.integer  "geo_country_id"
    t.integer  "geo_admin_id"
    t.integer  "geo_city_id"
    t.string   "geo_continent_name"
    t.string   "geo_admin_name"
    t.string   "geo_country_name"
    t.string   "geo_city_name"
    t.string   "geo_not_found"
    t.string   "contact_email"
    t.string   "wl_apple_url"
    t.string   "wl_android_url"
    t.string   "template",                                              default: "T1"
    t.boolean  "about_enabled",                                         default: false
    t.integer  "units"
    t.string   "property_types",                                        default: [],         array: true
    t.integer  "star_rating_id"
    t.string   "facilities",                                            default: [],         array: true
    t.string   "subscription_id"
    t.string   "next_plan"
    t.string   "previous_plan"
    t.datetime "plan_expiration_date"
    t.string   "zip_code"
    t.text     "responsive_websites"
    t.integer  "demo",                                                  default: 0
    t.boolean  "visible_to_web",                                        default: true
    t.boolean  "enable_admin_roles"
    t.boolean  "enable_custom_map",                                     default: false
    t.integer  "cloned_from"
    t.datetime "locking_date"
    t.boolean  "locked",                                                default: false
    t.datetime "locked_at"
    t.boolean  "enable_survey",                                         default: true
    t.boolean  "enable_directory",                                      default: true
    t.boolean  "enable_feedback",                                       default: true
    t.boolean  "enable_cart",                                           default: true
    t.boolean  "enable_home_automation",                                default: false
    t.boolean  "enable_person",                                         default: false
    t.boolean  "enable_hash_tag",                                       default: true
    t.boolean  "enable_page_item",                                      default: true
    t.boolean  "enable_service_request",                                default: false
    t.boolean  "enable_user_session",                                   default: true
    t.boolean  "enable_page_view",                                      default: true
    t.boolean  "enable_template",                                       default: true
    t.boolean  "enable_welcome_page",                                   default: true
    t.boolean  "enable_about_us",                                       default: true
    t.boolean  "enable_marketing_asset",                                default: false
    t.boolean  "enable_language",                                       default: true
    t.boolean  "enable_customer_email",                                 default: true
    t.boolean  "enable_customer_outreach",                              default: true
    t.boolean  "enable_booking_link",                                   default: true
    t.boolean  "enable_marketing_button",                               default: true
    t.string   "category",                                              default: "hotel"
    t.string   "service_cc"
    t.boolean  "enable_facility_booking",                               default: false
    t.string   "password_type",                                         default: "complex"
    t.boolean  "enable_email_logo",                                     default: true
    t.boolean  "enable_email_from",                                     default: false
    t.boolean  "enable_payment",                                        default: false
    t.boolean  "enable_voucher",                                        default: false
    t.boolean  "enable_order",                                          default: false
    t.boolean  "enable_booking",                                        default: false
    t.boolean  "enable_hashtag",                                        default: true
    t.boolean  "enable_messages",                                       default: true
    t.string   "home_automation_client_mode",                           default: "standard"
    t.boolean  "enable_ibeacon",                                        default: false
    t.boolean  "enable_member_login",                                   default: true
    t.boolean  "enable_member_register",                                default: true
    t.boolean  "enable_map",                                            default: true
  end

  add_index "appy_apps", ["appy_id"], name: "index_appy_apps_on_appy_id", using: :btree
  add_index "appy_apps", ["enabled_design_id"], name: "index_appy_apps_on_enabled_design_id", using: :btree
  add_index "appy_apps", ["mobile_domain_name"], name: "index_on_domain_name", using: :btree
  add_index "appy_apps", ["name"], name: "index_appy_apps_on_name", using: :btree
  add_index "appy_apps", ["user_id"], name: "index_appy_apps_on_user_id", using: :btree

  create_table "appy_apps_clients", force: true do |t|
    t.integer  "app_id"
    t.integer  "client_id"
    t.datetime "accessed_at"
    t.datetime "created_at"
  end

  add_index "appy_apps_clients", ["app_id"], name: "index_appy_apps_clients_on_app_id", using: :btree
  add_index "appy_apps_clients", ["client_id"], name: "index_appy_apps_clients_on_client_id", using: :btree

  create_table "appy_apps_devices", force: true do |t|
    t.integer  "app_id"
    t.integer  "device_id"
    t.datetime "accessed_at"
    t.datetime "created_at"
  end

  add_index "appy_apps_devices", ["app_id", "device_id"], name: "index_appy_apps_devices_on_app_id_and_device_id", unique: true, using: :btree

  create_table "appy_apps_people", force: true do |t|
    t.integer  "person_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "accessed_at"
  end

  create_table "appy_appy_identifiers", force: true do |t|
    t.integer  "identifier"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "identifier_alias"
  end

  add_index "appy_appy_identifiers", ["identifier"], name: "index_appy_appy_identifiers_on_identifier", using: :btree

  create_table "appy_automation_areas", force: true do |t|
    t.string   "code"
    t.string   "title"
    t.datetime "expired_at"
    t.string   "local_host",    default: "http://192.168.1.35"
    t.boolean  "permanent",     default: false
    t.boolean  "enabled",       default: true
    t.boolean  "deleted",       default: false
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remote_host"
    t.integer  "tahoma_box_id"
  end

  add_index "appy_automation_areas", ["app_id"], name: "index_appy_automation_areas_on_app_id", using: :btree

  create_table "appy_automation_areas_blueprints", force: true do |t|
    t.integer "area_id"
    t.integer "blueprint_id"
  end

  add_index "appy_automation_areas_blueprints", ["area_id"], name: "index_appy_automation_areas_blueprints_on_area_id", using: :btree
  add_index "appy_automation_areas_blueprints", ["blueprint_id"], name: "index_appy_automation_areas_blueprints_on_blueprint_id", using: :btree

  create_table "appy_automation_base_controls", force: true do |t|
    t.string   "title"
    t.string   "address",       default: "0"
    t.string   "interface",     default: "0"
    t.string   "control_type"
    t.boolean  "dimmer",        default: false
    t.string   "command",       default: "0"
    t.integer  "blueprint_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "icons"
    t.string   "type"
    t.string   "sound_channel"
    t.text     "positions"
  end

  add_index "appy_automation_base_controls", ["blueprint_id"], name: "index_appy_automation_base_controls_on_blueprint_id", using: :btree

  create_table "appy_automation_blueprints", force: true do |t|
    t.string   "title"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "master"
    t.integer  "position"
  end

  add_index "appy_automation_blueprints", ["app_id"], name: "index_appy_automation_blueprints_on_app_id", using: :btree

  create_table "appy_automation_cameras", force: true do |t|
    t.string   "title"
    t.string   "address",    default: "0"
    t.boolean  "enabled",    default: true
    t.boolean  "deleted",    default: false
    t.integer  "area_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ip_camera",  default: false
  end

  create_table "appy_automation_control_requests", force: true do |t|
    t.integer  "area_id"
    t.integer  "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_automation_control_requests", ["area_id"], name: "index_appy_automation_control_requests_on_area_id", using: :btree
  add_index "appy_automation_control_requests", ["device_id"], name: "index_appy_automation_control_requests_on_device_id", using: :btree

  create_table "appy_automation_coordinates", force: true do |t|
    t.string   "ratio"
    t.decimal  "x",               precision: 8, scale: 4, default: 0.5
    t.decimal  "y",               precision: 8, scale: 4, default: 0.5
    t.integer  "base_control_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_automation_coordinates", ["base_control_id"], name: "index_appy_automation_coordinates_on_base_control_id", using: :btree

  create_table "appy_automation_sources", force: true do |t|
    t.string   "title"
    t.string   "interface"
    t.integer  "sound_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_automation_tahoma_base_controls", force: true do |t|
    t.string   "control_id"
    t.string   "unique_hash"
    t.string   "title"
    t.string   "image_name"
    t.string   "central"
    t.text     "positions"
    t.text     "data"
    t.string   "type"
    t.integer  "tahoma_box_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_automation_tahoma_base_controls_rooms", force: true do |t|
    t.integer "tahoma_room_id"
    t.integer "tahoma_base_control_id"
  end

  create_table "appy_automation_tahoma_boxes", force: true do |t|
    t.string   "box_id"
    t.string   "title"
    t.string   "mac"
    t.string   "username"
    t.string   "password"
    t.text     "json"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_automation_tahoma_rooms", force: true do |t|
    t.string   "room_id"
    t.string   "unique_hash"
    t.string   "title"
    t.string   "image_name"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.integer  "tahoma_box_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_backend_usages", force: true do |t|
    t.string   "name"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_backend_usages", ["name", "app_id"], name: "index_appy_backend_usages_on_name_and_app_id", unique: true, using: :btree

  create_table "appy_base_content_translations", force: true do |t|
    t.string   "title",           null: false
    t.text     "description"
    t.text     "content"
    t.integer  "base_content_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "language_id"
    t.text     "hash_tags"
    t.text     "terms"
  end

  add_index "appy_base_content_translations", ["language_id"], name: "index_appy_base_content_translations_on_language_id", using: :btree

  create_table "appy_base_contents", force: true do |t|
    t.boolean  "enabled",                                     default: true
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.string   "category",                                    default: "content"
    t.decimal  "price",             precision: 16, scale: 2
    t.string   "price_per"
    t.string   "responder_email"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.string   "category_mode"
    t.text     "category_url"
    t.decimal  "lat",               precision: 18, scale: 15
    t.decimal  "lng",               precision: 18, scale: 15
    t.integer  "redeemable_min"
    t.integer  "redeemable_points"
    t.decimal  "redeemable_price",  precision: 16, scale: 2
    t.integer  "reward_points"
  end

  add_index "appy_base_contents", ["appable_id", "appable_type"], name: "index_appy_base_contents_on_appable_id_and_appable_type", using: :btree

  create_table "appy_booking_cart_items", force: true do |t|
    t.string   "title"
    t.decimal  "unit_price",     precision: 8, scale: 2
    t.integer  "quantity",                               default: 1
    t.string   "object_hash"
    t.integer  "cart_id"
    t.integer  "orderable_id"
    t.string   "orderable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_booking_cart_items", ["cart_id"], name: "index_appy_booking_cart_items_on_cart_id", using: :btree
  add_index "appy_booking_cart_items", ["orderable_id", "orderable_type"], name: "index_appy_booking_cart_orderable", using: :btree

  create_table "appy_booking_carts", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email"
    t.string   "customer_ref"
    t.text     "comment"
    t.string   "status"
    t.datetime "start_time"
    t.datetime "complete_time"
    t.date     "activated_date"
    t.decimal  "total_price",       precision: 10, scale: 2
    t.integer  "property_order_id"
    t.string   "category"
    t.boolean  "handled",                                    default: false
    t.boolean  "deleted",                                    default: false
    t.string   "object_hash"
    t.integer  "app_id"
    t.integer  "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "person_id"
    t.integer  "reservation_id"
    t.integer  "user_id"
    t.string   "type"
  end

  add_index "appy_booking_carts", ["app_id"], name: "index_appy_booking_carts_on_app_id", using: :btree
  add_index "appy_booking_carts", ["category"], name: "index_appy_booking_carts_on_category", using: :btree
  add_index "appy_booking_carts", ["person_id"], name: "index_appy_booking_carts_on_person_id", using: :btree
  add_index "appy_booking_carts", ["reservation_id"], name: "index_appy_booking_carts_on_reservation_id", using: :btree
  add_index "appy_booking_carts", ["user_id"], name: "index_appy_booking_carts_on_user_id", using: :btree

  create_table "appy_booking_reservations", force: true do |t|
    t.integer  "reservable_id"
    t.string   "reservable_type"
    t.datetime "start_at"
    t.integer  "duration"
    t.integer  "slots"
    t.boolean  "is_reservable",    default: false
    t.integer  "reservable_every"
    t.string   "week_type"
  end

  add_index "appy_booking_reservations", ["reservable_id", "reservable_type"], name: "index_appy_booking_reservations_on_reservable", using: :btree

  create_table "appy_client_translation_values", force: true do |t|
    t.hstore  "translation_value"
    t.integer "appy_client_translation_id"
  end

  create_table "appy_client_translations", force: true do |t|
    t.string   "key_translations"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "value_translations"
  end

  create_table "appy_clients", force: true do |t|
    t.integer  "device_id"
    t.integer  "mobile_application_id"
    t.datetime "created_at"
    t.integer  "person_id"
  end

  add_index "appy_clients", ["device_id"], name: "index_appy_clients_on_device_id", using: :btree
  add_index "appy_clients", ["mobile_application_id"], name: "index_appy_clients_on_mobile_application_id", using: :btree

  create_table "appy_content_item_translations", force: true do |t|
    t.string   "title",           null: false
    t.text     "description",     null: false
    t.integer  "content_item_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "language_id"
  end

  add_index "appy_content_item_translations", ["language_id"], name: "index_appy_content_item_translations_on_language_id", using: :btree

  create_table "appy_content_items", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "category",                                    default: "content"
    t.decimal  "price",              precision: 16, scale: 2
    t.integer  "base_content_id"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "position"
    t.string   "responder_email"
    t.string   "price_per"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.integer  "reward_points"
  end

  add_index "appy_content_items", ["appable_id", "appable_type"], name: "index_appy_content_items_on_appable_id_and_appable_type", using: :btree
  add_index "appy_content_items", ["base_content_id"], name: "index_appy_content_items_on_base_content_id", using: :btree

  create_table "appy_customer_emails", force: true do |t|
    t.string  "from_name"
    t.string  "subject",      limit: 1000
    t.text    "body"
    t.integer "appable_id"
    t.string  "appable_type"
    t.string  "from_address"
    t.text    "header"
    t.text    "footer"
  end

  add_index "appy_customer_emails", ["appable_id", "appable_type"], name: "index_appy_customer_emails_on_appable_id_and_appable_type", using: :btree

  create_table "appy_designs", force: true do |t|
    t.string   "name",                    null: false
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.integer  "position"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "appable_id"
    t.string   "appable_type"
    t.hstore   "colors"
  end

  add_index "appy_designs", ["appable_id", "appable_type"], name: "index_appy_designs_on_appable_id_and_appable_type", using: :btree

  create_table "appy_designs_menus", force: true do |t|
    t.integer "design_id"
    t.integer "menu_id"
    t.integer "position"
  end

  add_index "appy_designs_menus", ["design_id", "menu_id"], name: "index_appy_designs_menus_on_design_id_and_menu_id", unique: true, using: :btree
  add_index "appy_designs_menus", ["design_id"], name: "index_appy_designs_menus_on_design_id", using: :btree
  add_index "appy_designs_menus", ["menu_id"], name: "index_appy_designs_menus_on_menu_id", using: :btree

  create_table "appy_devices", force: true do |t|
    t.string   "name"
    t.string   "identifier"
    t.string   "bundle"
    t.string   "platform"
    t.string   "version"
    t.string   "screen_width"
    t.string   "screen_height"
    t.string   "ip_address"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_devices", ["identifier"], name: "index_appy_devices_on_identifier", unique: true, using: :btree

  create_table "appy_directory_contact_translations", force: true do |t|
    t.string   "name"
    t.text     "desc"
    t.string   "number"
    t.string   "area"
    t.integer  "contact_id"
    t.integer  "language_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_directory_contact_translations", ["contact_id"], name: "index_appy_directory_contact_translations_on_contact_id", using: :btree
  add_index "appy_directory_contact_translations", ["language_id"], name: "index_appy_directory_contact_translations_on_language_id", using: :btree

  create_table "appy_directory_contacts", force: true do |t|
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_directory_contacts", ["app_id"], name: "index_appy_directory_contacts_on_app_id", using: :btree

  create_table "appy_email_conditions", force: true do |t|
    t.string   "condition"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_email_conditions", ["condition"], name: "index_appy_email_conditions_on_condition", unique: true, using: :btree

  create_table "appy_email_logs", force: true do |t|
    t.integer  "app_id"
    t.integer  "email_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "emailable_id"
    t.string   "emailable_type"
  end

  add_index "appy_email_logs", ["app_id", "email_id"], name: "index_appy_email_logs_on_app_id_and_email_id", using: :btree

  create_table "appy_emails", force: true do |t|
    t.string   "name"
    t.string   "mailer_class"
    t.string   "mailer_method"
    t.boolean  "enabled",        default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "unsubscribable", default: false
    t.string   "send_to",        default: "Appy::App"
  end

  create_table "appy_emails_email_conditions", force: true do |t|
    t.integer "email_id"
    t.integer "email_condition_id"
  end

  add_index "appy_emails_email_conditions", ["email_id", "email_condition_id"], name: "emails_email_conditions_index", unique: true, using: :btree

  create_table "appy_facilities", force: true do |t|
    t.boolean  "enabled",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_facility_translations", id: false, force: true do |t|
    t.string   "iso_639_1",   limit: 2
    t.string   "iso_639_3",   limit: 3
    t.boolean  "default",                 default: false
    t.string   "name",        limit: 500
    t.integer  "facility_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_facility_translations", ["iso_639_1", "facility_id"], name: "iso_639_1_and_facility_id", unique: true, using: :btree

  create_table "appy_feedback_feedbacks", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "kind"
    t.text     "comment"
    t.boolean  "deleted",           default: false
    t.date     "activated_date"
    t.integer  "feedbackable_id"
    t.string   "feedbackable_type"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "read",              default: false
  end

  add_index "appy_feedback_feedbacks", ["app_id"], name: "app_in_feedbacks", using: :btree
  add_index "appy_feedback_feedbacks", ["feedbackable_id", "feedbackable_type"], name: "feedbackable_in_feedbacks", using: :btree

  create_table "appy_finance_omise_configurations", force: true do |t|
    t.integer  "appable_id"
    t.string   "omise_public_key"
    t.boolean  "deleted",          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "appable_type"
    t.string   "omise_vault_key"
  end

  create_table "appy_finance_omise_customers", force: true do |t|
    t.integer  "person_id"
    t.string   "customer_id"
    t.string   "card_reference"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "omise_configuration_id"
  end

  create_table "appy_finance_payments", force: true do |t|
    t.integer  "app_id"
    t.integer  "person_id"
    t.string   "category"
    t.integer  "amount",     default: 0
    t.boolean  "paid",       default: false
    t.datetime "paid_at"
    t.date     "due_at"
    t.boolean  "deleted",    default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "notes"
    t.string   "pid"
  end

  add_index "appy_finance_payments", ["app_id"], name: "index_appy_finance_payments_on_app_id", using: :btree
  add_index "appy_finance_payments", ["person_id"], name: "index_appy_finance_payments_on_person_id", using: :btree

  create_table "appy_geo_admins", id: false, force: true do |t|
    t.integer  "geonameid",        null: false
    t.string   "name"
    t.string   "asciiname"
    t.string   "country_code"
    t.string   "admin_code"
    t.integer  "parent_geonameid"
    t.string   "parent_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_geo_admins", ["country_code", "admin_code"], name: "index_appy_geo_admins_on_country_code_and_admin_code", using: :btree
  add_index "appy_geo_admins", ["country_code"], name: "index_appy_geo_admins_on_country_code", using: :btree

  create_table "appy_geo_cities", id: false, force: true do |t|
    t.integer  "geonameid",               null: false
    t.string   "name"
    t.string   "asciiname"
    t.text     "alternatenames"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "feature_class"
    t.string   "feature_code"
    t.string   "country_code"
    t.string   "cc2"
    t.string   "admin1_code"
    t.string   "admin2_code"
    t.string   "admin3_code"
    t.string   "admin4_code"
    t.integer  "population"
    t.integer  "elevation"
    t.integer  "dem"
    t.string   "timezone"
    t.datetime "modification"
    t.integer  "parent_geonameid"
    t.string   "parent_type"
    t.string   "type"
    t.string   "asciiname_first_letters"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_geo_cities", ["asciiname"], name: "index_appy_geo_cities_on_asciiname", using: :btree
  add_index "appy_geo_cities", ["country_code", "admin1_code"], name: "index_appy_geo_cities_on_country_code_and_admin1_code", using: :btree
  add_index "appy_geo_cities", ["country_code"], name: "index_appy_geo_cities_on_country_code", using: :btree
  add_index "appy_geo_cities", ["name"], name: "index_appy_geo_cities_on_name", using: :btree

  create_table "appy_geo_continents", id: false, force: true do |t|
    t.integer  "geonameid",  null: false
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_geo_continents", ["code"], name: "index_appy_geo_continents_on_code", using: :btree

  create_table "appy_geo_countries", id: false, force: true do |t|
    t.integer  "geonameid",            null: false
    t.string   "iso"
    t.string   "iso3"
    t.string   "iso_numeric"
    t.string   "fips"
    t.string   "country"
    t.string   "capital"
    t.integer  "area"
    t.integer  "population"
    t.string   "continent"
    t.string   "tld"
    t.string   "currency_code"
    t.string   "currency_name"
    t.string   "phone"
    t.string   "postal_code_format"
    t.string   "postal_code_regex"
    t.string   "languages"
    t.string   "neighbours"
    t.string   "equivalent_fips_code"
    t.integer  "parent_geonameid"
    t.string   "parent_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_geo_countries", ["iso"], name: "index_appy_geo_countries_on_iso", using: :btree

  create_table "appy_group_translations", force: true do |t|
    t.string   "description",          limit: 1000
    t.integer  "language_id"
    t.integer  "group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.string   "facebook_page_url"
    t.string   "twitter_handle"
    t.string   "booking_url"
    t.string   "booking_url_protocol"
  end

  add_index "appy_group_translations", ["group_id"], name: "index_appy_group_translations_on_group_id", using: :btree
  add_index "appy_group_translations", ["language_id"], name: "index_appy_group_translations_on_language_id", using: :btree

  create_table "appy_groups", force: true do |t|
    t.string   "name"
    t.integer  "owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "display_thumbnail",           default: false
    t.string   "mobile_domain_name"
    t.string   "wl_apple_url"
    t.string   "wl_android_url"
    t.string   "custom_text_highlight_color", default: "FFA800"
    t.string   "custom_background_color",     default: "000000"
    t.string   "custom_text_color",           default: "FFFFFF"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "image1_file_name"
    t.string   "image1_content_type"
    t.integer  "image1_file_size"
    t.datetime "image1_updated_at"
    t.string   "image2_file_name"
    t.string   "image2_content_type"
    t.integer  "image2_file_size"
    t.datetime "image2_updated_at"
    t.string   "contact_email"
    t.string   "category",                    default: "hotel"
    t.hstore   "colors"
  end

  add_index "appy_groups", ["mobile_domain_name"], name: "index_appy_groups_on_mobile_domain_name", using: :btree
  add_index "appy_groups", ["owner_id"], name: "index_appy_groups_on_owner_id", using: :btree

  create_table "appy_groups_apps", id: false, force: true do |t|
    t.integer "group_id"
    t.integer "app_id"
  end

  add_index "appy_groups_apps", ["group_id", "app_id"], name: "index_appy_groups_apps_on_group_id_and_app_id", unique: true, using: :btree

  create_table "appy_guest_guests_points", force: true do |t|
    t.integer  "points",       default: 0
    t.integer  "used_points",  default: 0
    t.date     "expiry_date"
    t.integer  "guest_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "spendable",    default: true
  end

  add_index "appy_guest_guests_points", ["appable_id", "appable_type"], name: "index_appy_guest_guests_points_on_appable_id_and_appable_type", using: :btree
  add_index "appy_guest_guests_points", ["expiry_date"], name: "index_appy_guest_guests_points_on_expiry_date", using: :btree
  add_index "appy_guest_guests_points", ["guest_id"], name: "index_appy_guest_guests_points_on_guest_id", using: :btree

  create_table "appy_guest_guests_statuses", force: true do |t|
    t.datetime "gained_at"
    t.datetime "expired_at"
    t.integer  "guest_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.integer  "status_id"
    t.boolean  "active",       default: true
  end

  add_index "appy_guest_guests_statuses", ["appable_id", "appable_type"], name: "index_appy_guest_guests_statuses_on_appable_id_and_appable_type", using: :btree
  add_index "appy_guest_guests_statuses", ["guest_id"], name: "index_appy_guest_guests_statuses_on_guest_id", using: :btree
  add_index "appy_guest_guests_statuses", ["status_id"], name: "index_appy_guest_guests_statuses_on_status_id", using: :btree

  create_table "appy_guest_loyalty_settings", force: true do |t|
    t.integer  "appable_id"
    t.string   "appable_type"
    t.integer  "points_expiry_value"
    t.integer  "points_expiry_type"
    t.integer  "tier_expiry_value"
    t.integer  "tier_expiry_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status_duration_type"
    t.integer  "tier_loading_period_value"
    t.integer  "tier_loading_period_type"
    t.boolean  "enable_loyalty",            default: false
    t.boolean  "publish_loyalty",           default: false
  end

  add_index "appy_guest_loyalty_settings", ["appable_id", "appable_type"], name: "loyalty_appable_index", using: :btree

  create_table "appy_guest_point_actions", force: true do |t|
    t.integer "appable_id"
    t.string  "appable_type"
    t.string  "name"
    t.integer "points"
    t.integer "frequency",    default: 0
  end

  add_index "appy_guest_point_actions", ["appable_id", "appable_type"], name: "index_appy_guest_point_actions_on_appable_id_and_appable_type", using: :btree

  create_table "appy_guest_statuses", force: true do |t|
    t.integer  "appable_id"
    t.string   "appable_type"
    t.string   "name"
    t.integer  "required_points"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.text     "description"
  end

  add_index "appy_guest_statuses", ["appable_id", "appable_type"], name: "status_appable_index", using: :btree

  create_table "appy_guest_voucher_redeems", force: true do |t|
    t.integer  "person_id"
    t.integer  "voucher_id"
    t.boolean  "redeemed",    default: false
    t.boolean  "boolean",     default: false
    t.boolean  "approved",    default: false
    t.boolean  "deleted",     default: false
    t.datetime "redeemed_at"
    t.datetime "created_at"
    t.string   "barcode"
  end

  add_index "appy_guest_voucher_redeems", ["person_id"], name: "index_appy_guest_voucher_redeems_on_person_id", using: :btree
  add_index "appy_guest_voucher_redeems", ["voucher_id"], name: "index_appy_guest_voucher_redeems_on_voucher_id", using: :btree

  create_table "appy_guest_vouchers", force: true do |t|
    t.integer  "app_id"
    t.string   "title"
    t.text     "content"
    t.text     "description"
    t.integer  "times_redeemed",           default: 0
    t.string   "redeem_type"
    t.integer  "redeem_value",   limit: 8
    t.string   "expire_type"
    t.integer  "expire_value"
    t.date     "expires_at"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.boolean  "deleted",                  default: false
    t.boolean  "boolean",                  default: false
    t.string   "cost_type"
    t.integer  "cost_value"
  end

  add_index "appy_guest_vouchers", ["app_id"], name: "index_appy_guest_vouchers_on_app_id", using: :btree

  create_table "appy_home_automation_bticino_cctvs", force: true do |t|
    t.string   "title"
    t.string   "address"
    t.boolean  "enabled",     default: true
    t.boolean  "deleted",     default: false
    t.integer  "position",    default: 0
    t.integer  "server_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "custom_data"
  end

  create_table "appy_home_automation_bticino_servers", force: true do |t|
    t.string   "title"
    t.string   "local_host"
    t.string   "remote_host"
    t.integer  "home_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "custom_data"
  end

  create_table "appy_home_automation_control_groups", force: true do |t|
    t.integer  "room_id"
    t.string   "title"
    t.integer  "position",    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "custom_data"
  end

  create_table "appy_home_automation_controls", force: true do |t|
    t.string   "title"
    t.string   "custom_icon_file_name"
    t.string   "custom_icon_content_type"
    t.integer  "custom_icon_file_size"
    t.datetime "custom_icon_updated_at"
    t.integer  "controllable_id"
    t.string   "controllable_type"
    t.string   "control_type"
    t.hstore   "custom_data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "default_icon_url"
    t.boolean  "use_custom_icon",          default: false
  end

  create_table "appy_home_automation_controls_control_groups", force: true do |t|
    t.integer  "control_id"
    t.integer  "control_group_id"
    t.integer  "position",         default: 0
    t.hstore   "custom_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_home_automation_controls_rooms", force: true do |t|
    t.integer  "control_id"
    t.integer  "room_id"
    t.hstore   "custom_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_home_automation_cool_automation_servers", force: true do |t|
    t.string   "title"
    t.string   "local_host"
    t.string   "local_port"
    t.string   "remote_host"
    t.string   "remote_port"
    t.integer  "home_id"
    t.hstore   "custom_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_home_automation_homes", force: true do |t|
    t.string   "code"
    t.string   "title"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.boolean  "enabled",                 default: true
    t.boolean  "deleted",                 default: false
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "custom_data"
  end

  create_table "appy_home_automation_rooms", force: true do |t|
    t.string   "title"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.integer  "position",                default: 0
    t.boolean  "enabled",                 default: true
    t.integer  "home_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "custom_data"
  end

  create_table "appy_home_automation_tahoma_remote_control_buttons", force: true do |t|
    t.string   "title"
    t.string   "custom_icon_file_name"
    t.string   "custom_icon_content_type"
    t.integer  "custom_icon_file_size"
    t.datetime "custom_icon_updated_at"
    t.integer  "remote_control_id"
    t.hstore   "custom_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_home_automation_tahoma_servers", force: true do |t|
    t.string   "title"
    t.string   "username"
    t.string   "password"
    t.string   "server_id"
    t.string   "mac"
    t.integer  "home_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "version"
    t.hstore   "custom_data"
  end

  create_table "appy_ibeacon_tracking_details", force: true do |t|
    t.datetime "active_time"
    t.string   "tracking_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "distance"
  end

  add_index "appy_ibeacon_tracking_details", ["tracking_id"], name: "index_appy_ibeacon_tracking_details_on_tracking_id", using: :btree
  add_index "appy_ibeacon_tracking_details", ["user_id"], name: "index_appy_ibeacon_tracking_details_on_user_id", using: :btree

  create_table "appy_ibeacon_trackings", force: true do |t|
    t.text     "message"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_active"
    t.string   "tracking_id"
  end

  create_table "appy_languages", force: true do |t|
    t.integer "number"
    t.string  "name"
    t.string  "iso_639_1"
    t.string  "iso_639_3"
    t.string  "iso_639_2b"
    t.string  "iso_639_2t"
    t.boolean "default"
    t.integer "position"
    t.integer "state"
    t.integer "appable_id"
    t.string  "appable_type"
  end

  add_index "appy_languages", ["state"], name: "index_appy_languages_on_state", using: :btree

  create_table "appy_map_coordinates", force: true do |t|
    t.string  "ratio"
    t.decimal "x",      precision: 8, scale: 4, default: 0.5
    t.decimal "y",      precision: 8, scale: 4, default: 0.5
    t.integer "pin_id"
  end

  create_table "appy_map_map_translations", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "map_id"
    t.integer  "language_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_map_map_translations", ["map_id", "language_id"], name: "index_appy_map_map_translations_on_map_id_and_language_id", using: :btree

  create_table "appy_map_maps", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "enabled"
    t.boolean  "publish_to_web"
    t.boolean  "publish_to_mobile"
    t.integer  "position"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "theme",              default: "dark"
  end

  create_table "appy_map_pin_translations", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "pin_id"
    t.integer  "language_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_map_pin_translations", ["pin_id", "language_id"], name: "index_appy_map_pin_translations_on_pin_id_and_language_id", using: :btree

  create_table "appy_map_pins", force: true do |t|
    t.integer  "target_id"
    t.string   "target_type"
    t.text     "url"
    t.integer  "map_id"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_map_pins", ["target_id", "target_type"], name: "index_appy_map_pins_on_target_id_and_target_type", using: :btree

  create_table "appy_media", force: true do |t|
    t.integer  "position"
    t.integer  "mediable_id"
    t.string   "mediable_type"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.text     "url"
    t.string   "host"
    t.string   "type",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "picture_processing"
    t.integer  "appable_id"
    t.string   "appable_type"
  end

  add_index "appy_media", ["appable_id", "appable_type"], name: "index_appy_media_on_appable_id_and_appable_type", using: :btree
  add_index "appy_media", ["mediable_id", "mediable_type"], name: "index_appy_media_on_mediable_id_and_mediable_type", using: :btree
  add_index "appy_media", ["type"], name: "index_appy_media_on_type", using: :btree

  create_table "appy_menu_translations", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.integer  "menu_id",     null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "language_id"
  end

  add_index "appy_menu_translations", ["language_id"], name: "index_appy_menu_translations_on_language_id", using: :btree

  create_table "appy_menus", force: true do |t|
    t.boolean  "enabled",                 default: true
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.integer  "accessible_from",         default: 2
    t.boolean  "editable",                default: true
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "deletable",               default: true
    t.boolean  "allow_disable",           default: true
    t.boolean  "custom",                  default: true
    t.string   "access_code"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.boolean  "featured",                default: false
    t.boolean  "publish_to_mobile",       default: true
    t.boolean  "publish_to_web",          default: true
  end

  add_index "appy_menus", ["appable_id", "appable_type"], name: "index_appy_menus_on_appable_id_and_appable_type", using: :btree

  create_table "appy_menus_base_contents", force: true do |t|
    t.integer "menu_id"
    t.integer "base_content_id"
    t.integer "position"
  end

  add_index "appy_menus_base_contents", ["base_content_id"], name: "index_appy_menus_base_contents_on_base_content_id", using: :btree
  add_index "appy_menus_base_contents", ["menu_id", "base_content_id"], name: "index_appy_menus_base_contents_on_menu_id_and_base_content_id", unique: true, using: :btree
  add_index "appy_menus_base_contents", ["menu_id"], name: "index_appy_menus_base_contents_on_menu_id", using: :btree

  create_table "appy_messages", force: true do |t|
    t.string   "title"
    t.string   "content"
    t.datetime "published_at"
    t.integer  "mobile_application_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "message_type",          default: "standard"
    t.boolean  "private",               default: false
    t.boolean  "deleted",               default: false
    t.integer  "appable_id"
    t.string   "appable_type"
    t.string   "send_method"
  end

  create_table "appy_mobile_applications", force: true do |t|
    t.string   "name"
    t.integer  "mobile_applicationable_id"
    t.string   "mobile_applicationable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "apple_pem_key"
    t.string   "apple_pem_passphrase"
    t.string   "android_gcm_key"
    t.integer  "daily_quota",                 default: 2
    t.boolean  "push_enable",                 default: false
    t.boolean  "enable_quota",                default: false
  end

  add_index "appy_mobile_applications", ["mobile_applicationable_id", "mobile_applicationable_type"], name: "index_appy_mobile_applications", using: :btree
  add_index "appy_mobile_applications", ["name"], name: "index_appy_mobile_applications_on_name", unique: true, using: :btree

  create_table "appy_mobile_applications_receivers", id: false, force: true do |t|
    t.integer "mobile_application_id"
    t.integer "receiver_id"
  end

  create_table "appy_outreach_filters", force: true do |t|
    t.integer "appable_id"
    t.string  "appable_type"
    t.string  "name"
    t.text    "filter"
  end

  add_index "appy_outreach_filters", ["appable_id", "appable_type"], name: "appy_outreach_filters_index", using: :btree

  create_table "appy_people", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.string   "locale"
    t.string   "age_range"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "birthdate"
    t.string   "address",             limit: 500
    t.string   "city"
    t.string   "country"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "phone"
    t.string   "tmp_password"
    t.string   "name"
    t.string   "unit"
  end

  add_index "appy_people", ["email"], name: "index_appy_people_on_email", unique: true, using: :btree
  add_index "appy_people", ["name"], name: "index_appy_people_on_name", using: :btree

  create_table "appy_person_customer_emails", force: true do |t|
    t.integer  "person_id"
    t.integer  "customer_email_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.datetime "sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_person_customer_emails", ["customer_email_id"], name: "index_appy_person_customer_emails_on_customer_email_id", using: :btree
  add_index "appy_person_customer_emails", ["person_id"], name: "index_appy_person_customer_emails_on_person_id", using: :btree

  create_table "appy_person_messages", force: true do |t|
    t.integer  "person_id"
    t.integer  "messageable_id"
    t.string   "messageable_type"
    t.boolean  "read",             default: false
    t.datetime "read_at"
    t.boolean  "private",          default: false
    t.string   "appable_type"
    t.integer  "appable_id"
    t.datetime "created_at"
  end

  add_index "appy_person_messages", ["messageable_id", "messageable_type"], name: "index_appy_person_messages_on_messageable", using: :btree
  add_index "appy_person_messages", ["person_id"], name: "index_appy_person_messages_on_person_id", using: :btree

  create_table "appy_property_type_translations", id: false, force: true do |t|
    t.string   "iso_639_1",        limit: 2
    t.string   "iso_639_3",        limit: 3
    t.boolean  "default",                      default: false
    t.string   "name",             limit: 500
    t.integer  "property_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_property_type_translations", ["iso_639_1", "property_type_id"], name: "iso_639_1_and_property_type_id", unique: true, using: :btree

  create_table "appy_property_types", force: true do |t|
    t.boolean  "enabled",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_publisher_apps", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.boolean  "active"
    t.text     "note"
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_publisher_apps", ["user_id"], name: "index_appy_publisher_apps_on_user_id", using: :btree

  create_table "appy_publisher_apps_apps", force: true do |t|
    t.integer  "publisher_app_id"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "forced"
  end

  add_index "appy_publisher_apps_apps", ["publisher_app_id", "app_id"], name: "index_appy_publisher_apps_apps_on_publisher_app_id_and_app_id", using: :btree

  create_table "appy_publisher_rules", force: true do |t|
    t.string   "entity_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "entity_value"
    t.boolean  "force_push",       default: false
    t.integer  "publisher_app_id"
  end

  add_index "appy_publisher_rules", ["entity_type"], name: "index_appy_publisher_rules_on_entity_type", using: :btree
  add_index "appy_publisher_rules", ["publisher_app_id"], name: "index_appy_publisher_rules_on_publisher_app_id", using: :btree

  create_table "appy_publisher_wizard_steps", force: true do |t|
    t.string   "title"
    t.integer  "position"
    t.string   "manager"
    t.string   "category"
    t.string   "clean_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_publisher_wizard_steps", ["category"], name: "index_appy_publisher_wizard_steps_on_category", using: :btree
  add_index "appy_publisher_wizard_steps", ["title"], name: "index_appy_publisher_wizard_steps_on_title", using: :btree

  create_table "appy_push_notification_receivers", force: true do |t|
    t.string   "token"
    t.string   "platform"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "accepted_content"
    t.integer  "device_id"
    t.boolean  "enable",                default: true
    t.integer  "person_id"
    t.integer  "mobile_application_id"
  end

  create_table "appy_roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_roles", ["name"], name: "index_appy_roles_on_name", using: :btree

  create_table "appy_session_statistics", force: true do |t|
    t.integer  "app_id"
    t.integer  "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
  end

  add_index "appy_session_statistics", ["app_id", "device_id"], name: "index_appy_session_statistics_on_app_id_and_device_id", using: :btree
  add_index "appy_session_statistics", ["app_id"], name: "index_appy_session_statistics_on_app_id", using: :btree
  add_index "appy_session_statistics", ["client_id"], name: "index_appy_session_statistics_on_client_id", using: :btree

  create_table "appy_sharing_statistics", force: true do |t|
    t.integer  "app_id"
    t.integer  "device_id"
    t.string   "statisticable_type"
    t.integer  "statisticable_id"
    t.string   "shared_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
  end

  add_index "appy_sharing_statistics", ["app_id", "device_id", "statisticable_type"], name: "index_on_sharing_statisticable_app_device", using: :btree
  add_index "appy_sharing_statistics", ["app_id", "statisticable_type"], name: "index_on_sharing_statisticable_and_app_id", using: :btree
  add_index "appy_sharing_statistics", ["client_id"], name: "index_appy_sharing_statistics_on_client_id", using: :btree
  add_index "appy_sharing_statistics", ["statisticable_id", "statisticable_type"], name: "index_on_sharing_statisticable", using: :btree

  create_table "appy_social_ids", force: true do |t|
    t.string  "social"
    t.string  "identifier"
    t.integer "person_id"
  end

  add_index "appy_social_ids", ["person_id"], name: "index_appy_social_ids_on_person_id", using: :btree
  add_index "appy_social_ids", ["social"], name: "index_appy_social_ids_on_social", using: :btree

  create_table "appy_star_rating_translations", id: false, force: true do |t|
    t.string   "iso_639_1",      limit: 2
    t.string   "iso_639_3",      limit: 3
    t.boolean  "default",                    default: false
    t.string   "name",           limit: 500
    t.integer  "star_rating_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_star_rating_translations", ["iso_639_1", "star_rating_id"], name: "iso_639_1_and_star_rating_id", unique: true, using: :btree

  create_table "appy_star_ratings", force: true do |t|
    t.boolean  "enabled",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_survey_question_translations", force: true do |t|
    t.text     "content"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "language_id"
  end

  add_index "appy_survey_question_translations", ["language_id"], name: "index_appy_survey_question_translations_on_language_id", using: :btree

  create_table "appy_survey_questions", force: true do |t|
    t.integer  "position"
    t.boolean  "deleted",    default: false
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "in_use",     default: true
  end

  add_index "appy_survey_questions", ["app_id"], name: "app_in_questions", using: :btree

  create_table "appy_survey_result_details", force: true do |t|
    t.integer  "result_id"
    t.integer  "question_id"
    t.integer  "score"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_survey_result_details", ["result_id", "question_id"], name: "survey_result_details", using: :btree

  create_table "appy_survey_results", force: true do |t|
    t.string   "surveyee_name"
    t.string   "surveyee_email"
    t.boolean  "deleted",        default: false
    t.string   "object_hash"
    t.integer  "app_id"
    t.integer  "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "activated_date"
  end

  add_index "appy_survey_results", ["app_id"], name: "app_in_survey_results", using: :btree

  create_table "appy_time_machine_app_states", force: true do |t|
    t.integer  "app_id"
    t.text     "app"
    t.text     "app_translations"
    t.text     "base_contents"
    t.text     "base_content_translations"
    t.text     "content_items"
    t.text     "content_item_translations"
    t.text     "designs"
    t.text     "menus"
    t.text     "menu_translations"
    t.text     "languages"
    t.text     "media"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_used_at"
    t.text     "designs_menus"
    t.text     "menus_base_contents"
    t.text     "directory_contacts"
    t.text     "directory_contact_translations"
    t.text     "survey_questions"
    t.text     "survey_question_translations"
    t.text     "wizard_apps_steps"
    t.string   "note"
  end

  add_index "appy_time_machine_app_states", ["app_id"], name: "index_appy_time_machine_app_states_on_app_id", using: :btree

  create_table "appy_tokens", force: true do |t|
    t.string   "kind"
    t.boolean  "from_plan",  default: true
    t.integer  "quantity"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_tokens", ["app_id", "kind", "from_plan"], name: "index_appy_tokens_on_app_id_and_kind_and_from_plan", unique: true, using: :btree
  add_index "appy_tokens", ["app_id"], name: "index_appy_tokens_on_app_id", using: :btree

  create_table "appy_users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "activation_token"
    t.string   "recovery_token"
    t.boolean  "signed_up",              default: false
    t.datetime "logged_at"
    t.boolean  "suspended",              default: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "work_title"
    t.string   "phone"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "failed_attempts",        default: 0
    t.string   "ip_address"
    t.boolean  "locked"
    t.datetime "locked_time"
    t.string   "login_country"
    t.string   "unlock_token"
    t.string   "billing_first_name"
    t.string   "billing_last_name"
    t.string   "billing_email"
    t.string   "billing_company"
    t.string   "billing_phone"
    t.string   "billing_address_line_1"
    t.string   "billing_address_line_2"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.string   "billing_country"
    t.text     "chargebee_ids",          default: [],    array: true
    t.integer  "soft_bounced",           default: 0
    t.integer  "hard_bounced",           default: 0
    t.string   "account_type"
  end

  add_index "appy_users", ["email"], name: "index_appy_users_on_email", unique: true, using: :btree
  add_index "appy_users", ["role_id"], name: "index_appy_users_on_role_id", using: :btree

  create_table "appy_users_apps", force: true do |t|
    t.integer "user_id"
    t.integer "app_id"
    t.string  "role"
  end

  add_index "appy_users_apps", ["user_id", "app_id"], name: "index_appy_users_apps_on_user_id_and_app_id", using: :btree

  create_table "appy_view_statistics", force: true do |t|
    t.integer  "app_id"
    t.integer  "device_id"
    t.string   "statisticable_type"
    t.integer  "statisticable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
  end

  add_index "appy_view_statistics", ["app_id", "device_id", "statisticable_type"], name: "index_on_view_statisticable_app_device", using: :btree
  add_index "appy_view_statistics", ["app_id", "statisticable_type"], name: "index_on_view_statisticable_and_app_id", using: :btree
  add_index "appy_view_statistics", ["client_id"], name: "index_appy_view_statistics_on_client_id", using: :btree
  add_index "appy_view_statistics", ["statisticable_id", "statisticable_type"], name: "index_on_view_statisticable", using: :btree

  create_table "appy_web_clients", force: true do |t|
    t.integer "appable_id"
    t.string  "appable_type"
    t.string  "domain_name"
  end

  create_table "appy_white_label_white_labels", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "keywords"
    t.string   "copyright"
    t.string   "support_url"
    t.string   "marketing_url"
    t.string   "policy_url"
    t.string   "landing_file_name"
    t.string   "landing_content_type"
    t.integer  "landing_file_size"
    t.datetime "landing_updated_at"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.integer  "user_id"
    t.integer  "appable_id"
    t.string   "appable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appy_wizard_apps_steps", force: true do |t|
    t.integer  "app_id"
    t.integer  "step_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appy_wizard_apps_steps", ["step_id", "app_id"], name: "index_appy_wizard_apps_steps_on_step_id_and_app_id", unique: true, using: :btree

  create_table "appy_wizard_steps", force: true do |t|
    t.string   "title"
    t.integer  "position"
    t.string   "manager"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "clean_title"
  end

  add_index "appy_wizard_steps", ["category"], name: "index_appy_wizard_steps_on_category", using: :btree
  add_index "appy_wizard_steps", ["title"], name: "index_appy_wizard_steps_on_title", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0
    t.integer  "attempts",   default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "simple_captcha_data", force: true do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

end
