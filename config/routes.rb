Rails.application.routes.draw do
  apipie
  scope module: 'shared' do
    root 'api#root'
  end

  concern :commandable do
    member do
      post :run
      get :run
    end
  end

  concern :statusable do
    member do
      get :status
    end
  end

  namespace :v1 do
    resources :homes, only: [:index], param: :home_id do
      member do
        resources :controls, only: [:index], concerns: [:commandable]
        resources :scenarios, only: [:index], concerns: [:commandable]
        resources :dimmers, only: [:index], concerns: [:commandable, :statusable]
        resources :blinds, only: [:index], concerns: [:commandable]
        resources :lights, only: [:index], concerns: [:commandable, :statusable]
        resources :air_conditioners, only: [:index], concerns: [:commandable]
        resources :outlets, only: [:index], concerns: [:commandable]
      end
    end
  end

  concern :v2_endpoints do
    resources :rooms, only: [:index, :show]
    resources :controls, only: [:index, :show]
    resources :scenarios, only: [:index], concerns: [:commandable]
    resources :dimmers, only: [:index], concerns: [:commandable, :statusable]
    resources :blinds, only: [:index], concerns: [:commandable]
    resources :lights, only: [:index], concerns: [:commandable, :statusable]
    resources :air_conditioners, only: [:index], concerns: [:commandable]
    resources :outlets, only: [:index], concerns: [:commandable]
    resources :cool_automations, only: [:index], concerns: [:commandable, :statusable], path: "cool_automations"
  end

  namespace :v2 do
    resources :homes, only: [:index, :show], param: :home_id do
      member do
        concerns :v2_endpoints
      end

      collection do
        concerns :v2_endpoints
      end
    end
  end

end
