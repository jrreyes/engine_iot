set :stage, :staging
set :application, 'appycontrol.staging'
set :app_path, "/var/www/#{fetch(:application)}"
set :branch, "staging"
set :bundle_without, [:development, :test]

# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server 'example.com', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}
server '139.162.4.97', port: 22, user: 'deploy', roles: %w{app db web}
set :deploy_to, -> { "#{fetch(:app_path)}" }

#puma
set :puma_bind,       "unix://#{fetch(:app_path)}/shared/sockets/puma.sock"
set :puma_state,      "#{fetch(:app_path)}/shared/pids/puma.state"
set :puma_pid,        "#{fetch(:app_path)}/shared/pids/puma.pid"
set :puma_error_log,  "#{fetch(:app_path)}/shared/log/error.log"
set :puma_access_log, "#{fetch(:app_path)}/current/log/access.log"
set :puma_workers, 1

#delayed job
set :delayed_job_prefix, :AppyControlStaging
set :delayed_job_workers, 1

#rvm
set :rvm_ruby_version, '2.1.0@appycontrol.staging'

