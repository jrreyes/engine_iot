module Bticino
  class Status

    attr_reader :status_message, :fetch_message, :value

    def initialize(status_message)
      ## status message should be in this format, *#WHO*VALUE*WHERE##
      @status_message = status_message
      @fetch_message = construct_fetch_message(status_message)
      @value = construct_value(status_message)
    end

    def construct_fetch_message(status_message)
      components = create_components(status_message)
      "*##{components[0]}*#{components[2]}##"
    end

    def construct_value(status_message)
      components = create_components(status_message)
      components[1]
    end

    def create_components(status_message)
      ## *#WHO*VALUE*WHERE## -> *WHO*VALUE*WHERE -> ['', WHO, VALUE, WHERE] -> [WHO, VALUE, WHERE]
      status_message.gsub("1000#", "").gsub("#", "").split("*").reject! {|v| !v.present?}
    end

    def status_for_control(control)
      control.try(:status_message) == @status_message
    end

  end
end