module Shared

  class Api

    def self.conf
      @@object ||= new
    end

    def self.enable_hashIds?
      ENV['HASH_IDS_SALT'].present?
    end

    def self.hashIds
      @hashIds ||= Hashids.new(ENV['HASH_IDS_SALT'], 8)
    end

    def initialize
      @conf = HashWithIndifferentAccess.new(YAML.load(File.read(Rails.root.join('config', 'api_config.yml'))))
    end

    def versions
      @conf.keys
    end

    def u(version)
      version.to_s.upcase
    end

    def deprecated?(version)
      @conf[u(version)]['deprecated']
    end

    def deprecation_date(version)
      @conf[u(version)]['deprecation_date']
    end

    def message(version)
      @conf[u(version)]['message']
    end

  end
end
