module CoolAutomation
  class Status

    attr_accessor :status_message, :uid, :state, :set_temp, :current_temp, :fan_speed, :mode, :failure_code

    def initialize(status_message)
      ## status message should be in this format, "L2. 101 ON 25C 27C Low Cool OK - 1"
      @status_message = status_message
      puts "Status >> #{status_message}\nComponents >> #{create_components(status_message)}"
      component = create_components(status_message)

      @uid = component[0]
      @state = component[1]
      @set_temp = component[2]
      @current_temp = component[3]
      @fan_speed = component[4]
      @mode = component[5]
      @failure_code = component[6]
    end

    def create_components(status_message)
      status_message.split(" ")
    end

  end
end