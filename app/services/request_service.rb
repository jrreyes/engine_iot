class RequestService

  def initialize(request, params)
    @request = request
    @params = params

    decorate_params
  end

  def app
    app_identifier.try(:app)
  end

  def home
    if id = decode(@params[:home_id])
      Appy::HomeAutomation::Home.find(id)
    elsif title = @params[:unit] || @params[:home_id]
      Appy::HomeAutomation::Home.where("title = ? OR alternate_title = ? OR code = ?", title, title, title).first
    else
      nil
    end
  end

  def app_identifier
    if @params[:app_key].present?
      Appy::AppyIdentifier.find_by(identifier_alias: @params[:app_key])
    else
      nil
    end
  end

  def loyalty_appable
    @loyalty_appable ||= app.belongs_to_loyalty_group? ? app.loyalty_group : app
  end

  def person
    @current_person ||= @params[:person_id] ? Appy::Person.find_by_id(@params[:person_id]) : nil
  end

  def present(name)
    "#{name}Presenter".constantize.new(send(name))
  end

  def to_hash
    @client_hash ||= {
      app: app,
      person: person
    }
  end

  def decode(hash)
    if Shared::Api.enable_hashIds?
      Shared::Api.hashIds.decode(hash).first
    else
      hash
    end
  end

  def encode(id)
    if Shared::Api.enable_hashIds?
      Shared::Api.hashIds.encode(id)
    else
      id
    end
  end

  private

  def decorate_params
    @params[:person_id] ||= (@params[:guest_id] || @params[:member_id])
  end

end
