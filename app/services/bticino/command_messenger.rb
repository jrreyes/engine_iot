module Bticino
  class CommandMessenger < Bticino::Messenger

  	attr_reader :control, :params

  	def initialize(control, params)
  		super(control.controllable.remote_host, params[:pin_code])

  		@control = control
  		@params = params
  	end

  	def command_action
  		@control.try(:what, params[:command])
  	end

  	def command_message
  		@control.try(:command_message, command_action)
  	end

  	#Override
  	def send(message = nil)
  		super(message || command_message)
  	end

  end
end