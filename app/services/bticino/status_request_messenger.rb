module Bticino
  class StatusRequestMessenger < Bticino::Messenger

  	attr_reader :control, :params, :data

  	def initialize(control, params)
  		super(control.controllable.remote_host, params[:pin_code])

  		@control = control
  		@params = params
  	end

  	def status_message
  		@control.try(:status_message)
  	end

  	#Override
  	def send(message = nil)
      @data = []
  		super(message || status_message) do |response|
        if !ACK_OR_NACK.include?(response)
          puts ">>>> Status Received #{response}\n\n"
          status = Bticino::StatusProcessor.new(response).call
          puts ">>>> Status Return #{status.try(:value)}"
          result = yield(control, status) if block_given? || nil

          if !result.nil?
            data << result
          end
        end

        @finished = true
      end
  	end

    def status
      @data
    end

  end
end