module Bticino
  class Messenger < BaseMessenger

    attr_accessor :pin, :finished

    ACK = "*#*1##"
    NACK = "*#*0##"
    ACK_OR_NACK = [ACK, NACK]

    def initialize(host, pin = nil)
      super(host, 20000)
      @pin = pin
    end

    def send(command_message)
      puts ">>>> Command : #{command_message} \n\n"
      @finished = false

      connect do |sock|
        read_until(sock, "##")
        sock.write "*99*0##"
        response = read_until(sock, "##")

        case response
        when NACK
          puts ">>>> Something's wrong\n\n"
          sock.close
          @result = Appy::Core::Error.new(:connection_refused)
        else
          if !authorized = (response == ACK)
            puts ">>>> PIN is require\n\n"
            open_password = calculate_open_password(@pin, response.gsub("*", "").gsub("#", ""))
            puts ">>>> Password calculated #{open_password} from pin #{@pin} and nouce #{response}\n\n"
            sock.write open_password
            response = read_until(sock, "##")
            puts response
            authorized = (response == ACK)
          end

          if authorized
            puts ">>>> Sending command #{command_message}\n\n"
            sock.write command_message

            if block_given?
              puts ">>>> Perform block code\n\n"
              while line = read_until(sock, "##")
                yield(line)
                
                if @finished
                  break
                end
              end
            else
              puts ">>>> Block is not given\n\n"
            end

            puts ">>>> Done\n\n"
            sock.close
            @result = Appy::Core::Success.new
          else
            puts ">>>> Unauthorized\n\n"
            sock.close
            @result = Appy::Core::Error.new(:connection_refused)
          end
        end
      end
    end

    def is_seed(response)
      response.match("\\*#[0-9]{5,10}##")
    end

    def calculate_open_password(password, nonce)
      if password.nil? || nonce.nil?
        return nil
      end

      msr = 0x7FFFFFFF;
      m_1 = 0xFFFFFFFF;
      m_8 = 0xFFFFFFF8;
      m_16 = 0xFFFFFFF0;
      m_128 = 0xFFFFFF80;
      m_16777216 = 0xFF000000;
      flag = true
	    num1 = 0x0
	    num2 = 0x0
      password = password.to_i

      nonce.to_s.chars.each do |c|
        num1 = num1 & m_1;
        num2 = num2 & m_1;

        if c != '0'
    			if flag
            num2 = password
          end
          flag = false
    		end

        puts "Before >> C: #{c}, Num 1: #{num1}, Num 2: #{num2}"
        case c
        when '1'
          num1 = num2 & m_128;
          num1 = num1 >> 1;
          num1 = num1 & msr;
          num1 = num1 >> 6;
          num2 = (num2 << 25) & m_1;
          num1 = num1 + num2;
        when '2'
          num1 = num2 & m_16;
          num1 = num1 >> 1;
          num1 = num1 & msr;
          num1 = num1 >> 3;
          num2 = (num2 << 28) & m_1;
          num1 = num1 + num2;
        when '3'
          nnum1 = num2 & m_8;
          num1 = num1 >> 1;
          num1 = num1 & msr;
          num1 = num1 >> 2;
          num2 = (num2 << 29) & m_1;
          num1 = num1 + num2;
        when '4'
          num1 = (num2 << 1) & m_1;
          num2 = num2 >> 1;
          num2 = num2 & msr;
          num2 = num2 >> 30;
          num1 = num1 + num2;
        when '5'
          num1 = (num2 << 5) & m_1;
          num2 = num2 >> 1;
          num2 = num2 & msr;
          num2 = num2 >> 26;
          num1 = num1 + num2;
        when '6'
          num1 = (num2 << 12) & m_1;
          num2 = num2 >> 1;
          num2 = num2 & msr;
          num2 = num2 >> 19;
          num1 = num1 + num2;
        when '7'
          num1 = num2 & 0xFF00;
          num1 = num1 + ((( num2 & 0xFF ) << 24 )) & m_1;
          num1 = num1 + (( num2 & 0xFF0000 ) >> 16 );
          num2 = num2 & m_16777216;
          num2 = num2 >> 1;
          num2 = num2 & msr;
          num2 = num2 >> 7;
          num1 = num1 + num2;
        when '8'
          num1 = num2 & 0xFFFF;
          num1 = (num1 << 16) & m_1;
          numx = num2 >> 1;
          numx = numx & msr;
          numx = numx >> 23;
          num1 = num1 + numx;
          num2 = num2 & 0xFF0000;
          num2 = num2 >> 1;
          num2 = num2 & msr;
          num2 = num2 >> 7;
          num1 = num1 + num2;
        when '9'
          num1 = ~num2
        when '0'
          num1 = num2
        end
        puts "After >>>> C: #{c}, Num 1: #{num1}, Num 2: #{num2}"
        num2 = num1
      end
      "*##{(num1 >> 0).to_s}##"
    end
  end

  def self.to_si(value, base, lenght = 32)
    mid = 2**(length-1)
    max_unsigned = 2**length
    n = value base
    (n>=mid) ? n - max_unsigned : n
  end
end
