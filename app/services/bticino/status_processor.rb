module Bticino
  class StatusProcessor

    def initialize(status)
      @status = status
    end

    def call
      Bticino::Status.new(@status)
    end

  end
end
