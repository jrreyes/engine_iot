require 'socket'

class BaseMessenger

  attr_accessor :host, :port, :timeout, :result

  def initialize(host, port, timeout = 3)
    @host = host
    @port = port
    @timeout = timeout
  end

  def connect
    begin
      @result = nil
      sock = Connect.new(@host, @port, @timeout).call
      yield(sock) if block_given?
    rescue Errno::ETIMEDOUT
      @result = Appy::Core::Error.new(:connection_timeout)
    rescue Errno::EHOSTUNREACH
      @result = Appy::Core::Error.new(:no_route_to_host)
    rescue Errno::EHOSTDOWN
      @result = Appy::Core::Error.new(:host_down)
    rescue Errno::ECONNREFUSED
      @result = Appy::Core::Error.new(:connection_refused)
    rescue => e
      puts ">>>> Error #{e}"
      @result = Appy::Core::Error.new(:unknown_error)
    end

    return @result
  end

  def send_success?
    !@result.nil? && @result.success?
  end

  def read_until(sock, suffix)
    response = ""
    while char = sock.read(1)
      response += char

      break if response.end_with?(suffix)
    end

    response
  end
end