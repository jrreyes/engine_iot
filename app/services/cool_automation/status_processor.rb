module CoolAutomation
  class StatusProcessor

    def initialize(response)
      @response = response
    end

    def call
      CoolAutomation::Status.new(@response)
    end

  end
end
