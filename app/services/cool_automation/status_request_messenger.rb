module CoolAutomation
  class StatusRequestMessenger < CoolAutomation::Messenger

  	attr_reader :control, :params, :data

  	def initialize(control, params)
  		super(control.controllable.remote_host, control.controllable.remote_port)

  		@control = control
  		@params = params
  	end

  	def status_message
  		@control.try(:status_message)
  	end

  	#Override
  	def send(message = nil)
      @data = []
  		super(message || status_message) do |response|
        if response.present?
          status = CoolAutomation::StatusProcessor.new(response).call
          result = yield(control, status) if block_given? || nil

          if !result.nil?
            data << result
          end
        end
      end
  	end

    def statuses
      @data
    end

  end
end