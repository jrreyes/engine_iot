module CoolAutomation
  class Messenger < BaseMessenger

    attr_accessor :finished
    attr_reader :responses, :exit_code

    def initialize(host, port)
      super(host, port)
    end

    def send(command_message)
      puts ">>>> Sending : #{command_message} \n\n"
      @finished = false

      connect do |sock|
        @responses = []
        sock.write "#{command_message}\n"
        puts ">>>> Sending command #{command_message}\n\n"
        while response = read_until(sock, "\n").gsub("\r", "").gsub("\n", "")
          response = process_response(response)
          
          puts ">>>> Response Line : #{response}\n\n"
          if EXIT_CODES.include?(response)
            @exit_code = response

            if response == SUCCESS_CODE
              @result = Appy::Core::Success.new
            else
              @result = Appy::Core::Error.new(:failed_to_send_command)
            end

            break
          else

            if block_given?
              puts ">>>> Perform block code\n\n"
              yield(response)

              if @finished
                break
              end
            else
              puts ">>>> Block is not given\n\n"
            end

            @responses << response
          end
        end
        sock.close

        if @result.nil?
          @result = Appy::Core::Success.new
        end
      end
    end

    def exit_code
      @exit_code
    end

    def responses
      @responses
    end

    def process_response(response)
      if response.present?
        response = response.strip

        if response.start_with?('>')
          response = response.slice(1..-1)
        end
      end

      response
    end
  end

  SUCCESS_CODE = "OK"
  EXIT_CODES = [
    SUCCESS_CODE,
    "No UID",
    "Not Strict UID",
    "Bad Format",
    "Failed",
    "Line Unused",
    "Unknown Command",
    "Bad HVAC Line",
    "Bad Function",
    "Bad Line Type",
    "Bad Parameter",
    "OK, Boot Required!",
    "Bad GPIO",
    "SDDP Disabled",
    "Virtual Address In Use",
    "Bad Property",
    "Number of lines exceeded",
    "Warning! Dip Switch State Incorrect",
    "SDDP Not Initialized",
    "ModBus Error:80",
    "ModBus Error:81",
    "ModBus Error:83",
    "ModBus Error:84",
    "ModBus Error:85",
    "ModBus Error:86",
    "Collision",
    "Unsupported Feature",
    "Incorrect Indoor Type",
    "No ACK From Indoor",
    "Time Out on Receive",
    "CS Error In Received Message",
    "Line Init In Progress...",
    "Line Error",
    "Feed Disabled",
    "HDL Not Initialized",
    "HDL DB Overflow",
    "HDL Eth Disabled",
    "UID Not Found",
    "Strict UID Not Found",
    "Indoor Removed",
    "DB Overflow",
    "Group DB Overflow",
    "VA DB Overflow",
    "FDB5 Overflow",
    "Link DB Overflow",
    "No CoolHub Line",
    "Auto Visibility Failed",
    "Link already exists",
    "KNX DB Overflow",
    "KNX Not Connected",
    "KNX Line Not Started"
  ]
end