module CoolAutomation
  class CommandMessenger < CoolAutomation::Messenger

  	 attr_reader :control, :params

  	def initialize(control, params)
  		super(control.controllable.remote_host, control.controllable.remote_port)

  		@control = control
  		@params = params
  	end

  	def command_action
  		@control.try(:command, params[:command])
  	end

  	def command_value 
  		params[:command_params]
  	end
       
    def command_message
    	@control.try(:command_message, command_action, command_value)
   	end

   	#Override
  	def send(message = nil)
  		super(message || command_message)
  	end

  end
end