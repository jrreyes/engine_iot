module V2
  class ScenePresenter < V2::ControlPresenter

    def as_json(*)
      json = super

      json[:scene_command] = internal_encode(control.command.to_i)

      json
    end

  end
end