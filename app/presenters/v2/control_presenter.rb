module V2
  class ControlPresenter < V2Presenter

    def self.present(control)
      case control
      when Appy::HomeAutomation::Bticino::Scene
        ScenePresenter.new(control)
      when Appy::HomeAutomation::Bticino::Dimmer
        DimmerPresenter.new(control)
      when Appy::HomeAutomation::Bticino::Blind
        BlindPresenter.new(control)
      when Appy::HomeAutomation::CoolAutomation::IndoorUnit
        IndoorUnitPresenter.new(control)
      else
        SwitchPresenter.new(control)
      end
    end

    def as_json(*)
      {
        id: encode(control.id),
        title: control.title,
        alternate_title: control.alternate_title,
        type: control.control_type_text,
        class: control.control_type.gsub("Appy::HomeAutomation::", ""),
        actions: control.try(:actions) || [],
        circuit_address: internal_encode(control_address),
        host_address: internal_encode(control.controllable.local_host.split(".").map {|r| r.to_i})
      }
    end

    def control
      @object
    end

    def control_address
      if control.interface == "0"
        [control.address.to_i]
      else
        [control.address.to_i, 4, "#{'%02d' % control.interface}".to_i]
      end
    end

    def internal_encode(val)
      Hashids.new("@ppysphere_dev", 8).encode(val)
    end

  end
end
