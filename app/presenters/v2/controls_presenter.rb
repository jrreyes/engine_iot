module V2
  class ControlsPresenter < V2Presenter

    def as_json(*)
      controls.collect { |control| ControlPresenter.present(control) }
    end

    def controls
      @object
    end

  end
end
