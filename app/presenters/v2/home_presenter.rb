module V2
  class HomePresenter < V2Presenter

    def as_json(*)
      data = {
        id: encode(home.id),
        title: home.title,
        alternate_title: home.alternate_title,
      }

      if @options[:with_rooms]
        data[:rooms] = RoomsPresenter.new(home.rooms)
      end

      data
    end

    def home
      @object
    end

  end
end
