module V2
  class CoolAutomationStatusPresenter < V2Presenter

    def initialize(object, status, options = {})
      super(object, options)

      @status = status
    end

    def as_json(*)
      {
        id: encode(control.id),
        title: control.title,
        alternate_title: control.alternate_title,
        state: status.state,
        set_temperature: status.set_temp,
        current_temperature: status.current_temp,
        fan_speed: status.fan_speed,
        mode: status.mode,
        failure_code: status.failure_code
      }
    end

    def control
      @object
    end

    def status
      @status
    end
  end
end
