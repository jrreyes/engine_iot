module V2
  class RoomsPresenter < V2Presenter

    def as_json(*)
      rooms.collect { |r| RoomPresenter.new(r, exclude_controls: true ) }
    end

    def rooms
      @object
    end

  end
end
