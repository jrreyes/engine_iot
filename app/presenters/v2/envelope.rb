module V2
  class Envelope < Shared::Envelope

    def as_json(*)
      json = {
        status: {
          code: @code,
          description: @description
        },
        api_status: {
          deprecated: Shared::Api.conf.deprecated?('v2').to_s,
          deprecation_date: Shared::Api.conf.deprecation_date('v2').to_s,
          message: Shared::Api.conf.message('v2').to_s
        },
        data:  @data || {}
      }

      @err ? json.merge!( error: @err ) : json
    end

  end
end
