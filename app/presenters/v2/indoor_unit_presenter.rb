module V2
  class IndoorUnitPresenter < V2::ControlPresenter

    def as_json(*)
      json = super
      json[:class] = json[:class].gsub("CoolAutomation", "CoolAC")

      json
    end

  end
end