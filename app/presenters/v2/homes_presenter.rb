module V2
  class HomesPresenter < V2Presenter

    def as_json(*)
      homes.collect { |home| HomePresenter.new(home) }
    end

    def homes
      @object
    end

  end
end