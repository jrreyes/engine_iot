module V2
  class RoomPresenter < V2Presenter

    def as_json(*)
      data = {
        id:                 encode(room.id),
        title:              room.title,
        alternate_title:    room.alternate_title,
        background:         Hash[Appy::HomeAutomation::Room::STYLES.map { |k,v| [k, room.background.url(k)] }],
      }

      data[:controls] = controls.map { |c| ControlPresenter.present(c) } unless @options[:exclude_controls]

      data
    end

    def room
      @object
    end

    def controls
      room.controls.where(id: room.home.api_controllable_controls.pluck(:id))
    end

  end
end
