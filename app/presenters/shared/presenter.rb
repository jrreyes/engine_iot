module Shared
  class Presenter

    def initialize(object, options = {})
      @object = object
      @options = options
    end

    def encode(id)
      if Shared::Api.enable_hashIds?
        Shared::Api.hashIds.encode(id)
      else
        id
      end
    end

  end
end