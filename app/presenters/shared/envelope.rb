module Shared
  class Envelope < Presenter

    def initialize(code, description, data, err = nil)
      @code = code
      @description = description
      @data = data
      @err = err
    end

  end
end
