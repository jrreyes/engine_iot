module V1
  class DimmerStatusPresenter < V1::ControlPresenter

    def initialize(object, status, options = {})
      super(object, options)

      @status = status
    end

    def as_json(*)
      {
        id: encode(control.id),
        title: control.title,
        min_value: "0",
        max_value: "10",
        current_value: value,
        last_action: last_action,
        available_actions: available_actions
      }
    end

    def value
      status.try(:value)
    end

    def status
      @status
    end

    def last_action
      Appy::HomeAutomation::Bticino::Dimmer::ACTIONS.select{|k, v| v == value}.keys.first
    end

    def available_actions
      Appy::HomeAutomation::Bticino::Dimmer::ACTIONS.select{|k, v| v != value}.keys
    end

  end
end
