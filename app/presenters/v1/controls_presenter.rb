module V1
  class ControlsPresenter < V1Presenter

    def as_json(*)
      controls.collect { |control| presenter_for_control(control) }
    end

    def controls
      @object
    end

    def presenter_for_control(control)
      case control
      when Appy::HomeAutomation::Bticino::Scene
        ScenePresenter.new(control)
      when Appy::HomeAutomation::Bticino::Dimmer
        DimmerPresenter.new(control)
      when Appy::HomeAutomation::Bticino::Blind
        BlindPresenter.new(control)
      else
        SwitchPresenter.new(control)
      end
    end

  end
end