module V1
  class HomePresenter < V1Presenter

    def as_json(*)
      {
        id: encode(home.id),
        title: home.title
      }
    end

    def home
      @object
    end

  end
end