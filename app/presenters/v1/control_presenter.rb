module V1
  class ControlPresenter < V1Presenter

    def as_json(*)
      {
        id: encode(control.id),
        title: control.title,
        type: control.control_type_text,
        class: control.control_type.gsub("Appy::HomeAutomation::", ""),
        actions: control.try(:actions) || []
      }
    end

    def control
      @object
    end

  end
end
