module V1
  class HomesPresenter < V1Presenter

    def as_json(*)
      homes.collect { |home| HomePresenter.new(home) }
    end

    def homes
      @object
    end

  end
end