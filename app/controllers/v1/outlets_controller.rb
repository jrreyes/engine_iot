module V1
  class OutletsController < V1::LightsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Outlet)
    end

  end
end
