module V1
  class AirConditionersController < V1::LightsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::AC)
    end

  end
end
