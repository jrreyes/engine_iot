module V1::Documentation::Shared::Base

  def headers
    V1::Documentation::Shared::Headers.doc(self)
  end

end
