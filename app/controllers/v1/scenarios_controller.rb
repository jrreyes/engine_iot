module V1
  class ScenariosController < V1::BticinoControlsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Scene)
    end

  end
end
