module V1
  class ControlsController < V1Controller

    expose(:control_id) { decode(params[:id]) }
    expose(:controls) do
      home.bticino_controls
    end
    expose(:control) do
      controls.find(control_id)
    end
    expose(:server) { control.controllable }

    def index
      ok ControlsPresenter.new(controls)
    end

    def run
      if control
        result = Bticino::CommandMessenger.new(control, params).send

        if result.success?
          ok
        else
          bad_request error: result.message
        end
      else
        not_found
      end
    end

  end
end
