module V1
  class HomesController < V1Controller

    def index
      if app
        ok HomesPresenter.new(app.homes.active)
      else
        not_found
      end
    end

  end
end
