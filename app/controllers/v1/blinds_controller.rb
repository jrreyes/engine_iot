module V1
  class BlindsController < V1::BticinoControlsController

		expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Blind)
    end

  end
end
