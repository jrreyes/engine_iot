module V1
  class V1Controller < Shared::ApiController

    extend V1::Documentation::Shared::Base

    def send_json(code, msg, data, err = nil)
      render(json: Envelope.new(code, msg, data, err).as_json, status: code) && return
    end

  end
end
