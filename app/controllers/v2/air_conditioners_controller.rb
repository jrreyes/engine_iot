module V2
  class AirConditionersController < V2::BticinoControlsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::AC)
    end

    include V2::Documentation::AirConditioners::Run
    def run
      super
    end

  end
end
