module V2
  class RoomsController < V2Controller

    expose(:room) { Appy::HomeAutomation::Room.find(decode(params[:id])) }

    include V2::Documentation::Rooms::Index
    def index
      if home
        ok RoomsPresenter.new(home.rooms)
      else
        not_found
      end
    end

    include V2::Documentation::Rooms::Show
    def show
      if room
        ok RoomPresenter.new(room)
      else
        not_found
      end
    end

  end
end
