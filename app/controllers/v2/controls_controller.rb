module V2
  class ControlsController < V2::V2Controller

    expose(:control_id) { decode(params[:id]) }
    expose(:controls) do
      home.api_controllable_controls
    end
    expose(:control) do
      controls.find(control_id)
    end
    expose(:server) { control.controllable }

    include V2::Documentation::Controls::Index
    def index
      ok ControlsPresenter.new(controls)
    end

    include V2::Documentation::Controls::Show
    def show
      ok ControlPresenter.present(control)
    end

  end
end
