module V2
  class LightsController < V2::BticinoControlsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Light)
    end

    include V2::Documentation::Lights::Run
    def run
      super
    end

    include V2::Documentation::Lights::Status
    def status
      if control
        srm = Bticino::StatusRequestMessenger.new(control, params)
        result = srm.send do |light, status|
          LightStatusPresenter.new(light, status).as_json
        end

        if result.success?
          ok srm.status
        else
          bad_request error: result.message
        end
      else
        not_found
      end
    end

  end
end
