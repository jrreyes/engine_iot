module V2
  class CoolAutomationsController < V2::ControlsController

    expose(:controls) do
      home.cool_automation_controls
    end

    include V2::Documentation::CoolAutomations::Status
    def status
      if control
        srm = CoolAutomation::StatusRequestMessenger.new(control, params)
        result = srm.send do |control, status|
          CoolAutomationStatusPresenter.new(control, status).as_json
        end

        if result.success?
          ok srm.statuses
        else
          bad_request error: result.message
        end
      else
        not_found
      end
    end

    include V2::Documentation::CoolAutomations::Run
    def run
      if control
        result = CoolAutomation::CommandMessenger.new(control, params).send

        if result.success?
          ok
        else
          bad_request error: result.message
        end
      else
        not_found
      end
    end

  end
end
