module V2
  class ScenariosController < V2::BticinoControlsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Scene)
    end

    include V2::Documentation::Scenarios::Run
    def run
      super
    end

  end
end
