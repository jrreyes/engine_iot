module V2::Documentation::Controls::Index
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/controls', 'Get all controls in a home'
    description <<-EOF
      You can also request only specific object type with following endpoint
        v2/homes/:home_id/lights
        v2/homes/:home_id/dimmers
        v2/homes/:home_id/blinds
        v2/homes/:home_id/scenarios
        v2/homes/:home_id/outlets
        v2/homes/:home_id/air_conditioners
        v2/homes/:home_id/cool_automations
    EOF
    param :home_id, String, "Home ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "id": "P9xXAZ9W",
                  "title": "Light 1",
                  "alternate_title": "Another Title",
                  "type": "Light",
                  "class": "Bticino::Light",
                  "actions": [
                      {
                          "name": "On",
                          "description": "Turn Light on",
                          "action": "CONTROL_ON"
                      },
                      {
                          "name": "off",
                          "description": "Turn Light on",
                          "action": "CONTROL_OFF"
                      }
                  ]
              },
              {
                  "id": "V1KY3EB7",
                  "title": "Dimmer 1",
                  "type": "Dimmer",
                  "class": "Bticino::Dimmer",
                  "actions": [
                      {
                          "name": "OFF",
                          "description": "Turn dimmer off",
                          "action": "DIMMER_OFF"
                      },
                      {
                          "name": "ON",
                          "description": "Turn dimmer on",
                          "action": "DIMMER_ON"
                      },
                      {
                          "name": "20%",
                          "description": "Set dimmer at 20%",
                          "action": "DIMMER_20"
                      },
                      {
                          "name": "30%",
                          "description": "Set dimmer at 30%",
                          "action": "DIMMER_30"
                      },
                      {
                          "name": "40%",
                          "description": "Set dimmer at 40%",
                          "action": "DIMMER_40"
                      },
                      {
                          "name": "50%",
                          "description": "Set dimmer at 50%",
                          "action": "DIMMER_50"
                      },
                      {
                          "name": "60%",
                          "description": "Set dimmer at 60%",
                          "action": "DIMMER_60"
                      },
                      {
                          "name": "70%",
                          "description": "Set dimmer at 70%",
                          "action": "DIMMER_70"
                      },
                      {
                          "name": "80%",
                          "description": "Set dimmer at 80%",
                          "action": "DIMMER_80"
                      },
                      {
                          "name": "90%",
                          "description": "Set dimmer at 90%",
                          "action": "DIMMER_90"
                      },
                      {
                          "name": "100%",
                          "description": "Set dimmer at 100%",
                          "action": "DIMMER_100"
                      }
                  ]
              },
              {
                  "id": "q9mR3l1A",
                  "title": "Blind 1",
                  "type": "Blind",
                  "class": "Bticino::Blind",
                  "actions": [
                      {
                          "name": "STOP",
                          "description": "Stop blind (Only for support hardware)",
                          "action": "BLIND_STOP"
                      },
                      {
                          "name": "On",
                          "description": "Turn blind on",
                          "action": "BLIND_ON"
                      },
                      {
                          "name": "off",
                          "description": "Turn blind off",
                          "action": "BLIND_OFF"
                      }
                  ]
              },
              {
                  "id": "ZBO0mw9q",
                  "title": "Outlet 1",
                  "type": "Outlet",
                  "class": "Bticino::Outlet",
                  "actions": [
                      {
                          "name": "On",
                          "description": "Turn Outlet on",
                          "action": "CONTROL_ON"
                      },
                      {
                          "name": "off",
                          "description": "Turn Outlet on",
                          "action": "CONTROL_OFF"
                      }
                  ]
              },
              {
                  "id": "X1Dx6g14",
                  "title": "AC 1",
                  "type": "AC",
                  "class": "Bticino::AC",
                  "actions": [
                      {
                          "name": "On",
                          "description": "Turn AC on",
                          "action": "CONTROL_ON"
                      },
                      {
                          "name": "off",
                          "description": "Turn AC on",
                          "action": "CONTROL_OFF"
                      }
                  ]
              },
              {
                  "id": "79zzxr9e",
                  "title": "Scene 1",
                  "type": "Scene",
                  "class": "Bticino::Scene",
                  "actions": [
                      {
                          "name": "Activate",
                          "description": "Activate predefine scenario",
                          "action": "ACTIVATE_SCENE"
                      }
                  ]
              }
          ]
      }
    EOS
  end
end
