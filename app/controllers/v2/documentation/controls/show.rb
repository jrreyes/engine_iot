module V2::Documentation::Controls::Show
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/controls/:id', 'Get control detail'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Control ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {
              "id": "P9xXAZ9W",
              "title": "Light 1",
              "alternate_title": "Another Title",
              "type": "Light",
              "class": "Bticino::Light",
              "actions": [
                  {
                      "name": "On",
                      "description": "Turn Light on",
                      "action": "CONTROL_ON"
                  },
                  {
                      "name": "off",
                      "description": "Turn Light on",
                      "action": "CONTROL_OFF"
                  }
              ]
          }
      }
    EOS
  end
end
