class V2::Documentation::Shared::Headers

  def self.doc(instance)
    lambda {
      instance.header 'appy_api_key', 'Appy API Key received from admin', required: true
      instance.header 'appy_app_key', 'Appy App Key received from admin', required: true
    }.call
  end

end
