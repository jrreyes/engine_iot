class V2::Documentation::Shared::Params

  def self.doc(instance)
    lambda {
      instance.param  'pin_code', String, desc: 'Gateway PIN'
    }.call
  end

end
