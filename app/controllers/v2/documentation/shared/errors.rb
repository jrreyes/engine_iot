class V2::Documentation::Shared::Errors

  def self.doc(instance)
    lambda {
      instance.error 400, "Incorrect parameters or header provided"
      instance.error 404, "Request resource not found"
      instance.error 500, "Something went wrong in the server, please report to administrator"
    }.call
  end

end

# instance.error 400, "Incorrect parameters or header provided", meta: <<-EOS
# {
#   "status":{
#     "code":400,
#     "description":"Bad Request"
#   },
#   "api_status":{
#     "deprecated":"false",
#     "deprecation_date":"",
#     "message":""
#   },
#   "data": {
#   }
# }
# EOS
# instance.error 404, "Request resource not found", meta: <<-EOS
# {
#   "status":{
#     "code":404,
#     "description":"Not Found"
#   },
#   "api_status":{
#     "deprecated":"false",
#     "deprecation_date":"",
#     "message":""
#   },
#   "data": {
#   }
# }
# EOS
# instance.error 500, "Something went wrong in the server, please report to administrator", meta: <<-EOS
# {
#   "status":{
#     "code":500,
#     "description":"Error"
#   },
#   "api_status":{
#     "deprecated":"false",
#     "deprecation_date":"",
#     "message":""
#   },
#   "data": {
#   }
# }
# EOS
