module V2::Documentation::Shared::Base

  def basic_config
    headers
    params
    errors
  end

  def headers
    V2::Documentation::Shared::Headers.doc(self)
  end

  def params
    V2::Documentation::Shared::Params.doc(self)
  end

  def errors
    V2::Documentation::Shared::Errors.doc(self)
  end

end
