module V2::Documentation::Scenarios::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/scenarios/:id/run', 'Run scenario command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Scenario ID", required: true
    param :command, ["ACTIVATE_SCENE"], "Scenario command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
