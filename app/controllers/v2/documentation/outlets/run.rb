module V2::Documentation::Outlets::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/outlets/:id/run', 'Run outlet command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Outlet ID", required: true
    param :command, ["CONTROL_ON", "CONTROL_OFF"], "Outlet command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
