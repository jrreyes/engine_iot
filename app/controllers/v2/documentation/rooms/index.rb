module V2::Documentation::Rooms::Index
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/rooms', 'Get all rooms in a home'
    param :home_id, String, "Home ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "id": "Y81dVyBb",
                  "title": "Somfy",
                  "alternate_title": "Another Title",
                  "background": {
                      "original": "/backgrounds/original/missing.png",
                      "ah_xxlarge": "/backgrounds/ah_xxlarge/missing.png",
                      "ah_xlarge": "/backgrounds/ah_xlarge/missing.png",
                      "ah_large": "/backgrounds/ah_large/missing.png",
                      "ah_medium": "/backgrounds/ah_medium/missing.png",
                      "ah_small": "/backgrounds/ah_small/missing.png",
                      "ah_thumb": "/backgrounds/ah_thumb/missing.png"
                  }
              }
          ]
      }
    EOS
  end
end
