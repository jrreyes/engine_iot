module V2::Documentation::Rooms::Show
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/rooms/:id', 'Get room detail'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Room ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {
              "id": "Y81dVyBb",
              "title": "Somfy",
              "alternate_title": "Another Title",
              "background": {
                  "original": "/backgrounds/original/missing.png",
                  "ah_xxlarge": "/backgrounds/ah_xxlarge/missing.png",
                  "ah_xlarge": "/backgrounds/ah_xlarge/missing.png",
                  "ah_large": "/backgrounds/ah_large/missing.png",
                  "ah_medium": "/backgrounds/ah_medium/missing.png",
                  "ah_small": "/backgrounds/ah_small/missing.png",
                  "ah_thumb": "/backgrounds/ah_thumb/missing.png"
              },
              "controls": [
                  {
                      "id": "79zXqPje",
                      "title": "Strip Light 1",
                      "type": "Dimmer",
                      "class": "Bticino::Dimmer",
                      "actions": [
                          {
                              "name": "OFF",
                              "description": "Turn dimmer off",
                              "action": "DIMMER_OFF"
                          },
                          {
                              "name": "ON",
                              "description": "Turn dimmer on",
                              "action": "DIMMER_ON"
                          },
                          {
                              "name": "20%",
                              "description": "Set dimmer at 20%",
                              "action": "DIMMER_20"
                          },
                          {
                              "name": "30%",
                              "description": "Set dimmer at 30%",
                              "action": "DIMMER_30"
                          },
                          {
                              "name": "40%",
                              "description": "Set dimmer at 40%",
                              "action": "DIMMER_40"
                          },
                          {
                              "name": "50%",
                              "description": "Set dimmer at 50%",
                              "action": "DIMMER_50"
                          },
                          {
                              "name": "60%",
                              "description": "Set dimmer at 60%",
                              "action": "DIMMER_60"
                          },
                          {
                              "name": "70%",
                              "description": "Set dimmer at 70%",
                              "action": "DIMMER_70"
                          },
                          {
                              "name": "80%",
                              "description": "Set dimmer at 80%",
                              "action": "DIMMER_80"
                          },
                          {
                              "name": "90%",
                              "description": "Set dimmer at 90%",
                              "action": "DIMMER_90"
                          },
                          {
                              "name": "100%",
                              "description": "Set dimmer at 100%",
                              "action": "DIMMER_100"
                          }
                      ]
                  },
                  {
                      "id": "21QGzyBv",
                      "title": "Fan",
                      "type": "AC",
                      "class": "Bticino::AC",
                      "actions": [
                          {
                              "name": "On",
                              "description": "Turn AC on",
                              "action": "CONTROL_ON"
                          },
                          {
                              "name": "off",
                              "description": "Turn AC on",
                              "action": "CONTROL_OFF"
                          }
                      ]
                  },
                  {
                      "id": "71nxOXBO",
                      "title": "Strip Light 2",
                      "type": "Dimmer",
                      "class": "Bticino::Dimmer",
                      "actions": [
                          {
                              "name": "OFF",
                              "description": "Turn dimmer off",
                              "action": "DIMMER_OFF"
                          },
                          {
                              "name": "ON",
                              "description": "Turn dimmer on",
                              "action": "DIMMER_ON"
                          },
                          {
                              "name": "20%",
                              "description": "Set dimmer at 20%",
                              "action": "DIMMER_20"
                          },
                          {
                              "name": "30%",
                              "description": "Set dimmer at 30%",
                              "action": "DIMMER_30"
                          },
                          {
                              "name": "40%",
                              "description": "Set dimmer at 40%",
                              "action": "DIMMER_40"
                          },
                          {
                              "name": "50%",
                              "description": "Set dimmer at 50%",
                              "action": "DIMMER_50"
                          },
                          {
                              "name": "60%",
                              "description": "Set dimmer at 60%",
                              "action": "DIMMER_60"
                          },
                          {
                              "name": "70%",
                              "description": "Set dimmer at 70%",
                              "action": "DIMMER_70"
                          },
                          {
                              "name": "80%",
                              "description": "Set dimmer at 80%",
                              "action": "DIMMER_80"
                          },
                          {
                              "name": "90%",
                              "description": "Set dimmer at 90%",
                              "action": "DIMMER_90"
                          },
                          {
                              "name": "100%",
                              "description": "Set dimmer at 100%",
                              "action": "DIMMER_100"
                          }
                      ]
                  }
              ]
          }
      }
    EOS
  end
end
