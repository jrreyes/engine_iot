module V2::Documentation::Dimmers::Status
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/dimmers/:id/status', 'Request dimmer status'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Dimmer ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "Id": "V1KY3EB7",
                  "title": "Dimmer",
                  "alternate_title": "Another Title",
                  "min_value": "0",
                  "max_value": "10",
                  "current_value": "6",
                  "last_action": "DIMMER_60",
                  "available_actions": [
                      "DIMMER_OFF",
                      "DIMMER_ON",
                      "DIMMER_10",
                      "DIMMER_20",
                      "DIMMER_30",
                      "DIMMER_40",
                      "DIMMER_50",
                      "DIMMER_70",
                      "DIMMER_80",
                      "DIMMER_90",
                      "DIMMER_100"
                  ]
              }
          ]
      }
    EOS
  end
end
