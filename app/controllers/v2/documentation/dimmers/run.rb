module V2::Documentation::Dimmers::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/dimmers/:id/run', 'Run dimmer command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Dimmer ID", required: true
    param :command, ["DIMMER_OFF", "DIMMER_ON", "DIMMER_20", "DIMMER_30", "DIMMER_40", "DIMMER_50", "DIMMER_60", "DIMMER_70", "DIMMER_80", "DIMMER_90", "DIMMER_100"], "Dimmer command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
