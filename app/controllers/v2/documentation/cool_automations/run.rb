module V2::Documentation::CoolAutomations::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/cool_automations/:id/run', 'Run cool automation command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Cool Automation ID", required: true
    param :command, ["CONTROL_ON", "CONTROL_OFF", "AC_MODE_COOL", "AC_MODE_FAN", "AC_SET_TEMP", "AC_SET_FAN_SPEED", "AC_SET_FAN_SWING"], "Cool Automation command", required: true
    param :command_params, String, required: false, desc: <<-EOF
      If command is AC_SET_TEMP
        Value is the desire temperature
      If command is AC_SET_FAN_SPEED
        Value is one of following

        V or v (very low)
        L or l (low)
        M or m (medium)
        H or h (high)
        T or t (very high)
        A or a (auto)
      If command is AC_SET_FAN_SWING
        Value is one of following

        H or h (horizontal)
        V or v (vertical)
        A or a (auto)
    EOF
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
