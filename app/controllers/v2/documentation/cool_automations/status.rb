module V2::Documentation::CoolAutomations::Status
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/cool_automations/:id/status', 'Request cool automation status'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Cool Automation ID", required: true
    example <<-EOS
      {
          "status": {
             "code": 200,
             "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "id": "1234",
                  "title": "Daikin",
                  "alternate_title": "Another Title",
                  "state": "ON",
                  "set_temperature": "17C",
                  "current_temperature": "31C",
                  "fan_speed": "Auto",
                  "mode": "Cool",
                  "failure_code": "OK"
              }
          ]
      }
    EOS
  end
end
