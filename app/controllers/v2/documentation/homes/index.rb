module V2::Documentation::Homes::Index
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes', 'Get all homes belongs to an app'
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "id": "6ZBO5k1q",
                  "title": "Appy Office",
                  "alternate_title": "Another Title",
              },
              {
                  "id": "NV1P57jy",
                  "title": "Somfy",
                  "alternate_title": "Another Title",
              },
              {
                  "id": "379z83je",
                  "title": "iHome",
                  "alternate_title": "Another Title",
              }
          ]
      }
    EOS
  end
end
