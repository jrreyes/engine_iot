module V2::Documentation::AirConditioners::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/air_conditioners/:id/run', 'Run air conditioner command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Air Conditioner ID", required: true
    param :command, ["CONTROL_ON", "CONTROL_OFF"], "Air Conditioner command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
