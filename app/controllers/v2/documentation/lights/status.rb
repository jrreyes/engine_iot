module V2::Documentation::Lights::Status
  extend ActiveSupport::Concern

  included do
    basic_config

    api :GET, 'v2/homes/:home_id/lights/:id/status', 'Request light status'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Light ID", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": [
              {
                  "Id": "P9xXAZ9W",
                  "title": "Light",
                  "alternate_title": "Another Title",
                  "min_value": "0",
                  "max_value": "1",
                  "current_value": "0",
                  "last_action": "CONTROL_OFF",
                  "available_actions": [
                      "CONTROL_ON"
                  ]
              }
          ]
        }
      EOS
  end
end
