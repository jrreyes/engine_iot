module V2::Documentation::Lights::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/lights/:id/run', 'Run light command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Light ID", required: true
    param :command, ["CONTROL_ON", "CONTROL_OFF"], "Light command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
