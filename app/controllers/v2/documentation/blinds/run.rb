module V2::Documentation::Blinds::Run
  extend ActiveSupport::Concern

  included do
    basic_config

    api :POST, 'v2/homes/:home_id/blinds/:id/run', 'Run blind command'
    param :home_id, String, "Home ID", required: true
    param :id, String, "Blind ID", required: true
    param :command, ["BLIND_ON", "BLIND_OFF", "BLIND_STOP"], "Blind command", required: true
    example <<-EOS
      {
          "status": {
              "code": 200,
              "description": "OK"
          },
          "api_status": {
              "deprecated": "false",
              "deprecation_date": "",
              "message": ""
          },
          "data": {}
      }
    EOS
  end
end
