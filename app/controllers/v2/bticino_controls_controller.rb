module V2
  class BticinoControlsController < V2::ControlsController

  	def run
      if control
        result = Bticino::CommandMessenger.new(control, params).send

        if result.success?
          ok
        else
          bad_request error: result.message
        end
      else
        not_found
      end
    end

  end
end
