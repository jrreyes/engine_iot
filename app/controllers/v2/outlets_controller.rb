module V2
  class OutletsController < V2::BticinoControlsController

    expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Outlet)
    end

    include V2::Documentation::AirConditioners::Run
    def run
      super
    end

  end
end
