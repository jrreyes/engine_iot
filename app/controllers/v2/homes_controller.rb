module V2
  class HomesController < V2Controller

    include V2::Documentation::Homes::Index

    def index
      if app
        ok HomesPresenter.new(app.homes.active)
      else
        not_found
      end
    end

    def show
      if home
        ok HomePresenter.new(home, with_rooms: true)
      else
        not_found
      end
    end

  end
end
