module V2
  class BlindsController < V2::BticinoControlsController

		expose(:controls) do
      home.bticino_controls.where(control_type: Appy::HomeAutomation::Bticino::Blind)
    end

    include V2::Documentation::Blinds::Run
    def run
      super
    end

  end
end
