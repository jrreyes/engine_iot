module Shared

  class ApiController < ActionController::API

    resource_description do
      api_version "1.0", "2.0"
      app_info "Appy Control API"
    end

    expose(:app) { rs.app }
    expose(:home) { rs.home }

    before_filter :prepare

    def root; end

    def options
      ok
    end

    protected

    def request_service
      @request_service ||= init_request_service
    end
    alias_method :rs, :request_service

    def reset_request_service
      @request_service = init_request_service
    end

    def init_request_service
      RequestService.new(request, params)
    end

    def decode(hash)
      rs.decode(hash)
    end

    def encode(id)
      rs.encode(id)
    end

    def status(code, msg)
      render json: { status: code, message: msg }, status: code
    end

    def send_json(code, msg, data, err = nil)
      render(json: Envelope.new(code, msg, data, err).as_json, status:  code) && return
    end

    def ok(data = {})
      send_json(200, 'OK', data)
    end

    def no_content(data = {})
      send_json(204, 'No Content', data)
    end

    def not_modified(data = {})
      send_json(304, 'Not Modified', data)
    end

    def bad_request(data = {}, err = nil)
      send_json(400, 'Bad Request', data, err)
    end

    def unauthorized(data = {})
      send_json(401, 'Unauthorized', data)
    end

    def gone(data = {})
      send_json(410, 'Gone', data)
    end

    def not_found(data = {})
      send_json(404, 'Not Found', data)
    end

    def internal_error(data = {})
      send_json(500, 'Error', data)
    end

    def xhr_options?
      request.headers['X-Appy-Agent'] || request.user_agent =~ /^Appy Hotel\// || request.request_method == "OPTIONS"
    end

    def xhr_post?
      request.request_method == "POST"
    end

    def cross_origin
      response.headers['Access-Control-Allow-Origin'] = '*'
      response.headers['Access-Control-Allow-Methods'] = 'OPTIONS, GET, POST'
      response.headers['Access-Control-Allow-Headers'] = 'X-Appy-Agent, Content-Type'
    end

    def api_key
      Appy::ApiKey.with_key(params[:api_key]).first
    end

    private

    def prepare
      params[:app_key] ||= request.headers[:HTTP_APPY_APP_KEY]
      params[:api_key] ||= (request.headers[:HTTP_APPY_API_KEY] || params[:key])
      params[:person_id] ||= (request.headers[:HTTP_APPY_MEMBER_ID] || params[:member_id] || params[:user_id])

      protect_with_api_key
      cross_origin if xhr_options?
    end

    def protect_with_api_key
      unauthorized if api_key.nil?
    end

  end

end
